import Notes from "./Notes";

function Home() {
  return (
    <>
      <h1>Welcome visitor!</h1>
      <p>
        If you're reading this, I'm guessing you're as passionate about React as
        I am. Or maybe you're just curious. Either way, welcome!
      </p>
      <Notes />
    </>
  );
}

export default Home;
