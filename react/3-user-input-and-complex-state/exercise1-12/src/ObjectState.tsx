import { useState } from "react";

function ObjectState() {
  const [objectState, setObjectState] = useState({
    count: 0,
    userInputText: "",
  });

  function handleCount() {
    const newObjectState = {
      ...objectState,
      count: (objectState.count += 1),
      userInputText: "",
    };
    setObjectState(newObjectState);
  }

  function handleInput(e: React.ChangeEvent<HTMLInputElement>) {
    const newObjectState = {
      ...objectState,
      userInputText: e.target.value,
    };
    setObjectState(newObjectState);
  }

  return (
    <>
      <input
        type="text"
        value={objectState.userInputText}
        onChange={handleInput}
      />
      <button className="btn" onClick={handleCount}>
        {objectState.count}
      </button>
    </>
  );
}

export default ObjectState;
