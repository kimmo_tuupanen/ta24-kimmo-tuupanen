interface MyDogProps {
  name: string;
  age: number;
  sex: string;
}

function MyDog({ name, age, sex }: MyDogProps) {
  function calcDogAge(dogAge: number): number {
    let i = 1;
    let dogAgeInHumanYears = 15;
    while (i <= dogAge) {
      if (i === 2) {
        dogAgeInHumanYears += 9;
      } else if (i > 2 && i <= 5) {
        dogAgeInHumanYears += 4;
      } else if (i === 6) {
        dogAgeInHumanYears += 9;
      } else if (i > 6 && i < 16) {
        dogAgeInHumanYears += 5;
      } else if (i === 16) {
        dogAgeInHumanYears = 120;
        console.log("Your dog should be dead already!");
      }
      i++;
    }
    return dogAgeInHumanYears;
  }

  return (
    <p>
      My dogs name is {name} and {sex === "male" ? "he" : "she"} is{" "}
      {calcDogAge(age)} years old in human years.
    </p>
  );
}

export default MyDog;
