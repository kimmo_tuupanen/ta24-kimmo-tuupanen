const arr: number[] = [4, 19, 7, 1, 9, 22, 6, 13];

function findLargest(arr: number[]) {
  let max = arr.at(0);
  for (const item of arr) {
    if (item >= max!) max = item;
  }
  return max;
}

const largest = findLargest(arr);

console.log(largest); // prints 22
