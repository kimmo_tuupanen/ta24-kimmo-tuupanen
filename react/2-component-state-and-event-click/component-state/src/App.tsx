import "./App.css";
import Box from "./Box";
import Button from "./Button";
import Buttons from "./Buttons";
import { useEffect, useState } from "react";
import { checkForBingo } from "./checkForBingo";

const names = [
  "Anakin Skywalker",
  "Leia Organa",
  "Han Solo",
  "C-3PO",
  "R2-D2",
  "Darth Vader",
  "Obi-Wan Kenobi",
  "Yoda",
  "Palpatine",
  "Boba Fett",
  "Lando Calrissian",
  "Jabba the Hutt",
  "Mace Windu",
  "Padmé Amidala",
  "Count Dooku",
  "Qui-Gon Jinn",
  "Aayla Secura",
  "Ahsoka Tano",
  "Ki-Adi-Mundi",
  "Luminara Unduli",
  "Plo Koon",
  "Kit Fisto",
  "Shmi Skywalker",
  "Beru Whitesun",
  "Owen Lars",
];

export interface GridObject {
  name: string;
  isClicked: boolean;
}

function App() {
  const [showText, setShowText] = useState(true);
  const [grid, setGrid] = useState<GridObject[][]>([]);

  useEffect(() => {
    const gridSideLength = 5;
    const newGrid: GridObject[][] = [];

    for (let i = 0; i < gridSideLength; i++) {
      const row: GridObject[] = [];
      for (let j = 0; j < gridSideLength; j++) {
        const index = i * gridSideLength + j;
        row.push({
          name: names[index],
          isClicked: false,
        });
      }
      newGrid.push(row);
    }

    setGrid(newGrid);
  }, []);

  const handleBoxClick = (rowIndex: number, colIndex: number) => {
    const updatedGrid = grid.map((row, i) =>
      row.map((box, j) => {
        if (i === rowIndex && j === colIndex) {
          return { ...box, isClicked: true };
        }
        return box;
      })
    );

    setGrid(updatedGrid);
    if (checkForBingo(updatedGrid)) {
      console.log("Bingo!");
    }
  };

  return (
    <>
      <h1 className="heading-primary">Assignment 1: Toggle render</h1>
      <Button setShowText={setShowText} />
      {showText && (
        <p>Difficulty is signal of growth, failure is part of growth.</p>
      )}

      <h1 className="heading-primary">Assignment 2: More toggle render</h1>
      <Buttons />

      <h1 className="heading-primary">Assignment 3: Bingo</h1>
      {/* BINGO */}
      {grid.map((row, rowIndex) => (
        <div key={rowIndex}>
          {row.map((box, colIndex) => (
            <Box
              key={colIndex}
              name={box.name}
              onClick={() => handleBoxClick(rowIndex, colIndex)}
              isClicked={box.isClicked}
            />
          ))}
        </div>
      ))}
    </>
  );
}

export default App;
