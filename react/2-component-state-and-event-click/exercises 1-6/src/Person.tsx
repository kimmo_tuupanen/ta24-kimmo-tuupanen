import styled from "styled-components";

interface PersonProps {
  name: string;
  age: number;
}

const Text = styled.p`
  font-size: 1.1rem;
  font-weight: 500;
  color: gainsboro;
`;

function Person({ name, age }: PersonProps) {
  return (
    <Text>
      {name} is {age} years old!
    </Text>
  );
}

export default Person;
