import express from "express";
import noteRouter from "./noteRouter";

export const app = express();
app.use(express.json());
app.use("/notes", noteRouter);

// app.use((err, req, res, next) => {
//   console.error(err.stack);
//   res.status(500).json({ message: "URL Not Found" });
// });

if (!process.env["VITE"]) {
  const frontendFiles = process.cwd() + "/dist";
  app.use(express.static(frontendFiles));
  app.get("/*", (_, res) => {
    res.send(frontendFiles + "/index.html");
  });
  app.listen(process.env["PORT"]);
}
