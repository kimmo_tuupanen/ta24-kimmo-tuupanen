{
  const numbers = [4, 7, 1, 8, 5];

  // a)
  function incrementAll(numArr: number[]) {
    const newArr = numArr.map((num) => (num += 1));
    return newArr;
  }

  const newNumbers1 = incrementAll(numbers);
  console.log(newNumbers1); // prints [ 5, 8, 2, 9, 6 ]

  // b)
  function decrementAll(numArr: number[]) {
    const newArr = numArr.map((num) => (num -= 1));
    return newArr;
  }

  const newNumbers2 = decrementAll(numbers);
  console.log(newNumbers2); // prints [ 3, 6, 0, 7, 4 ]
}
