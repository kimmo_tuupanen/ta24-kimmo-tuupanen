import express from "express";

const server = express();

server.get("/", (request, response) => {
  response.send("Hello world!");
});

server.get("/endpoint2", (request, response) => {
  response.send("End of the world!");
});

server.listen(3000, () => {
  console.log("Listening to port 3000");
});
