const sentence = "use this text reverser to quickly reverse text";
const reversed = reverseWords(sentence);

function reverseWords(sentence: string): string {
  const words = sentence.split(" ");
  const reversedWords: string[] = [];

  for (const word of words) {
    const reversedLetters: string[] = [];
    let letters = word.split("");

    for (let i = letters.length - 1; i >= 0; i--) {
      reversedLetters.push(letters[i]);
    }

    reversedWords.push("_", reversedLetters.join(""));
  }

  const reversedSentence = reversedWords
    .toString()
    .replace("_,", "")
    .replace(/,_,/g, " ");

  return reversedSentence;
}

console.log(reversed);
