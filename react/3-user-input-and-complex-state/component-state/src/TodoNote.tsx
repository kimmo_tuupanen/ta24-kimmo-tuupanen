import { useState } from "react";

type TodoNoteProps = {
  todo: { id: string; text: string; complete: boolean };
  onChange: (id: string) => void;
  onEdit: (id: string, newText: string) => void;
  onDelete: (id: string) => void;
};

function TodoNote({ todo, onEdit, onChange, onDelete }: TodoNoteProps) {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [inputText, setInputText] = useState(todo.text);
  const { id, text, complete } = todo;

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    setInputText(e.target.value);
  }

  return (
    <>
      <div
        className="todoNote"
        style={
          complete
            ? { backgroundColor: "aquamarine" }
            : { backgroundColor: "darkseagreen" }
        }
      >
        <input
          type="checkbox"
          checked={complete}
          onChange={() => onChange(id)}
        />
        {!editMode ? (
          <p>{text}</p>
        ) : (
          <input
            className="input"
            type="text"
            placeholder={inputText}
            onChange={handleInputChange}
          />
        )}

        <button className="btn-delete" onClick={() => onDelete(id)}>
          X
        </button>
        <div style={{ width: "100%" }}>
          {!editMode ? (
            <button className="btn" onClick={() => setEditMode(true)}>
              Edit
            </button>
          ) : (
            <button
              className="btn"
              onClick={() => {
                onEdit(id, inputText);
                setEditMode(false);
              }}
            >
              Save
            </button>
          )}
        </div>
      </div>
    </>
  );
}

export default TodoNote;
