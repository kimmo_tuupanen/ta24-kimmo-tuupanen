function stringCount(str: string) {
  const strLength = str.length;
  const strWords = str.split(" ").length;
  return { length: strLength, words: strWords };
}

console.log(stringCount("The fox was already in your chicken house"));
