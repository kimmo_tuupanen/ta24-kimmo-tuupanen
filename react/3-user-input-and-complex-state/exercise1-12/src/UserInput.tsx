import { useState } from "react";

// E2
const UserInput = () => {
  const [userInput, setUserInput] = useState("");
  const [displayText, setDisplayText] = useState("");

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput(e.target.value);
  };

  const onSubmit = () => {
    setDisplayText(userInput);
    setUserInput("");
  };

  return (
    <>
      <h3>Your string is: {displayText}</h3>
      <input type="text" value={userInput} onChange={handleChange}></input>
      <button className="btn" onClick={onSubmit}>
        Submit
      </button>
    </>
  );
};

export default UserInput;
