type ButtonProps = {
  setShowText: React.Dispatch<React.SetStateAction<boolean>>;
};

function Button({ setShowText }: ButtonProps) {
  return (
    <button
      className="btn"
      onClick={() => {
        setShowText((show) => !show);
      }}
    >
      Toggle text
    </button>
  );
}

export default Button;
