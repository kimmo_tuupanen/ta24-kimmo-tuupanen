// a)

class Room {
  constructor(public width: number, public height: number) {}

  furniture: string[] = [];

  getArea() {
    return this.width * this.height;
  }

  addFurniture(item: string) {
    this.furniture.push(item);
  }
}

const room = new Room(4.5, 6.0);
console.log(room); // Room { width: 4.5, height: 6 }

// b)

const area = room.getArea();
console.log(area); // prints 27

// c)

room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");
console.log(room); // prints Room { width: 4.5, height: 6, furniture: [ 'sofa', 'bed', 'chair' ] }
