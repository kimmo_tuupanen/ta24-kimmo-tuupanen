const button = document.getElementById("click_button");
let click = 1;

const clicks = () => {
  return click++;
};

button.addEventListener("click", () => {
  window.alert(`You have clicked the button ${clicks()} times!`);
});

function changeColor() {
  document.querySelector(".square").style.backgroundColor = "red";
}

function hello() {
  setTimeout(() => {
    const h1Element = document.querySelector("h1");
    if (h1Element) {
      h1Element.textContent += " hello world!";
    }
  }, 1000);
}
hello();
