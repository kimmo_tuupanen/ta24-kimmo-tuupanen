// 1
// because sum operand has left-associativity

// 2
const text3 = "monkeys " + (6 + 6); // "monkeys 12"
console.log(text3);