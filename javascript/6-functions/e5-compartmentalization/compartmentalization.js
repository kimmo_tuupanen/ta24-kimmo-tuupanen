const tree = { name: "Tree", x: 6, y: 7, hitpoints: 30 };
const rock = { name: "Rock", x: 3, y: 11, hitpoints: 90 };
const damage = 15;

let treeHitpointsLeft;
let rockHitpointsLeft;

// 1.
function damageTree(dmg) {
    treeHitpointsLeft = tree.hitpoints - dmg;
    console.log("Tree hitpoints left: " + treeHitpointsLeft);
}

function damageRock(dmg) {
    rockHitpointsLeft = rock.hitpoints - dmg;
    console.log("Rock hitpoints left: " + rockHitpointsLeft);
}
damageTree(damage);
damageRock(damage);

// 2.
function totalDamage(target, dmg) {
    let hitpointsLeft = target.hitpoints - dmg;
    console.log(`${target.name} hitpoints left: ${hitpointsLeft}`);
}
totalDamage(tree, damage);

    

