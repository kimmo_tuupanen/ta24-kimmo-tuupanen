// 2 a)
let number;
const result1 = 10 + number; // NaN
// number is type of undefined, so adding up 10 and undefined results Not-A-Number

number = null;
const result2 = 10 + number; // 10
// number assigned to null makes result2 -> 10 + null = 10


// 2 b)
const a = true;
const b = false;

const c = a + b; // 1
const d = 10 + a; // 11
const e = 10 + b; // 10


// Here is how primitive values are converted to numbers:
// const myTruthy = Number(true); // 1
// const myFalsy = Number(false); // 0
