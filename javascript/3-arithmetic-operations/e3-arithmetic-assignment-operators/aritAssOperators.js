// Create a program and assign a number to variables n.
let n = 8;

// Use addition assignment operator to add 5 to the n
n += 5;
console.log(n);

// Use multiplication assignment operator to multiply n with 2
n *= 2;
console.log(n);

// Use subtraction assignment operator to subtract 5 from n
n -= 5;
console.log(n);

// Use divide assignment operator to divide n with 2
n /= 2;
console.log(n);

// Print answers from all the operations using console.log
