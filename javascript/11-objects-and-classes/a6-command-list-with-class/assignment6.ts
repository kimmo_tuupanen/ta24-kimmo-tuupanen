{
  interface CommandHandlers {
    N: () => void;
    E: () => void;
    S: () => void;
    W: () => void;
  }

  class Robot {
    public commandHandlers: CommandHandlers;
    public x: number;
    public y: number;

    constructor() {
      this.x = 0;
      this.y = 0;

      this.commandHandlers = {
        N: () => (this.y = this.y + 1),
        E: () => (this.x = this.x + 1),
        S: () => (this.y = this.y - 1),
        W: () => (this.x = this.x - 1),
      };
    }

    handleCommandList(list: string) {
      for (const command of list) {
        if (command === "B") break;
        if (command === "C") continue;
        this.commandHandlers[command as keyof CommandHandlers]();
      }
      console.log(`Current position of robot x = ${this.x}, y = ${this.y}`);
    }
  }

  const robot = new Robot();
  const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";
  robot.handleCommandList(commandList);
  console.log(robot);
}
