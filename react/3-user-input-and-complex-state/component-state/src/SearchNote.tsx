type SearchNoteProps = {
  setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
};

function SearcNote({ setSearchQuery }: SearchNoteProps) {
  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    setSearchQuery(e.target.value);
  }

  return (
    <div>
      <input
        className="input"
        type="text"
        placeholder="Search for a note..."
        onChange={(e) => handleInputChange(e)}
      />
    </div>
  );
}

export default SearcNote;
