const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };


function calcTriangleArea(triangle) {
    return triangle.width * triangle.length / 2;   
}

console.log("Area of first triangle: " + calcTriangleArea(firstTriangle));
console.log("Area of second triangle: " + calcTriangleArea(secondTriangle));
console.log("Area of third triangle: " + calcTriangleArea(thirdTriangle));

// EXTRA: Create another function that figures out which triangle had the biggest area and prints it. For example, if the area of the third triangle was biggest, it'd print "Third triangle has biggest area!". Call your function at the end of the code.

function biggestTriangle(tri1, tri2, tri3) {
    const areaOne = calcTriangleArea(tri1);
    const areaTwo = calcTriangleArea(tri2);
    const areaThree = calcTriangleArea(tri3);

    if(areaOne >= areaTwo && areaOne >= areaThree) {
        console.log("First triangle has biggest area!")
    } else if(areaTwo >= areaThree) {
        console.log("Second triangle has biggest area!")
    } else {
        console.log("Second triangle has biggest area!")
    }
}

biggestTriangle(firstTriangle, secondTriangle, thirdTriangle);