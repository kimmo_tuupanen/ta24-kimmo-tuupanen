const greet = document.getElementById("greeting");
const form = document.getElementById("form");

greet.textContent += "Hello anonymous!";

form.addEventListener("submit", (e) => {
  e.preventDefault();

  const username = document.getElementById("username");
  greet.textContent = `Hello ${username.value}!`;
});
