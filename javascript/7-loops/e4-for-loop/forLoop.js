// 1
let sum = 0;
let numberSequence = "";

for(let i = 0; i <= 10; i++) {
    numberSequence += ` ${sum}`;
    sum += + 100;
}
// console.log(numberSequence);

// 2
sum = 1;
numberSequence = "";
for(let i = 1; i <= 8; i++) {
    numberSequence += ` ${sum}`;
    sum += sum;
}
// console.log(numberSequence);

// 3
let add = 3;
sum = 0; 
numberSequence = "";
for(let i = 1; i <= 5; i++) {
    sum += add; 
    numberSequence += ` ${sum}`;
}
// console.log(numberSequence);

// 4
sum = 9;
numberSequence = "";
for(let i = 1; i <= 10; i++) {
    numberSequence += ` ${sum}`;
    sum += - 1; 
}
// console.log(numberSequence);

// 5
sum = 1;
numberSequence = "";
for(let i = 1; i <= 4; i++) {
    numberSequence += ` ${sum} ${sum} ${sum}`;
    sum += + 1;
}
// console.log(numberSequence);

// 6
sum = 0;
numberSequence = "";
let totalSequence = "";
for(let i = 1; i <= 5; i++) {
    numberSequence += `${sum} `; 
    totalSequence = `${numberSequence}${numberSequence}${numberSequence}`;
    sum += + 1; 
}
console.log(totalSequence);