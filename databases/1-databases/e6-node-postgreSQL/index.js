import express from "express";
import { createProductsTable } from "./db.js";
// import router from "./router.js";

const server = express();

createProductsTable();

const { PORT } = process.env;
server.listen(PORT, () => {
  console.log("Products API listening to port", PORT);
});
