import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

import About from "./About";
import Home from "./Home";
import Links from "./Links";
import "./App.css";

function App() {
  return (
    <Router>
      <div>
        <Link className="main-nav-link" to="/">
          Home
        </Link>
        <Link className="main-nav-link" to="/about">
          About
        </Link>
        <Link className="main-nav-link" to="/links">
          Links
        </Link>
      </div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/links" element={<Links />} />
      </Routes>
    </Router>
  );
}

export default App;
