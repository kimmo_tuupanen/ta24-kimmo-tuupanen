import axios from "axios";
import { URL } from "../jsonplaceholder.js";

async function getPostComments(id: number) {
  try {
    const response = await axios.get(`${URL}/posts/${id}/comments`);
    console.log(response.data);
  } catch (error) {
    console.error(error);
  }
}

getPostComments(5);
