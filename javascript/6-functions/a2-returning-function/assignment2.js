function minimum(num1, num2, num3) {
    let smallest;
    if(num1 <= num2 && num1 <= num3) {
        smallest = num1;
    } else if(num2 <= num3) {
        smallest = num2;
    } else {
        smallest = num3;
    }
    return smallest;
}

console.log(minimum(27, 13, 49));
console.log(minimum(7, 132, 55));
console.log(minimum(5, 9, 3));