// 1 )
import axios from "axios";
import { URL } from "../jsonplaceholder";

function postItem() {
  axios
    .post(`${URL}/posts/`, {
      title: "My first post",
      body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    })
    .then((response) => {
      console.log(response.data);
      console.log(`HTTP status code: ${response.status}`);
    })
    .catch((error) => console.error(error));
}

postItem();

// 2)
