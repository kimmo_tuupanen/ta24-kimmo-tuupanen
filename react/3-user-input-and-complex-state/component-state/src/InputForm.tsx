import { useState } from "react";

type InputFormProps = { addTodo: (text: string) => void };

function InputForm({ addTodo }: InputFormProps) {
  const [newTodo, setNewTodo] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    if (newTodo === "") return;

    e.preventDefault();
    addTodo(newTodo);
    setNewTodo("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        className="input"
        type="text"
        value={newTodo}
        placeholder="Add a new note"
        onChange={(e) => setNewTodo(e.target.value)}
      />
      <button className="btn" type="submit">
        Add note
      </button>
    </form>
  );
}

export default InputForm;
