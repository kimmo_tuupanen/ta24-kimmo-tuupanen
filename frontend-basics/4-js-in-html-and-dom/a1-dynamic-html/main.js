const students = [
  { name: "Sami", score: 24.75 },
  { name: "Heidi", score: 20.25 },
  { name: "Jyrki", score: 27.5 },
  { name: "Helinä", score: 26.0 },
  { name: "Maria", score: 17.0 },
  { name: "Yrjö", score: 14.5 },
];

const toggleButton = document.getElementById("toggle");
let container = document.querySelector(".container");

toggleButton.addEventListener("click", () => {
  let studentsTable = "";
  if (container.innerHTML === "") {
    studentsTable = `
  <table>
    <caption>
      Front-end web developer course 2024
    </caption>
    <thead>
      <tr>
      <th>Person</th>
      <th>Score</th>
      </tr>
    </thead>
    <tbody>
      ${students
        .map(
          (student) => `
      <tr>
        <th>${student.name}</th>
        <td>${student.score}</td>
      </tr>                                                                                                             `
        )
        .join("")}
    </tbody>
  </table>                                                                                                                                 `;
  } else studentsTable = "";

  container.innerHTML = studentsTable;
});
