import React from "react";
import styled from "styled-components";

const BingoBox = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  color: darkslategray;
  height: 3rem;
  width: 10rem;
  margin: 0.3rem;
  border: 1px solid darkgray;
  border-radius: 4px;
  padding: 0.5rem 1rem;
  cursor: pointer;
`;

interface BingoBoxProps {
  name: string;
  onClick: () => void;
  isClicked: boolean;
}

const Box: React.FC<BingoBoxProps> = (props: BingoBoxProps) => {
  const { name, onClick, isClicked } = props;

  return (
    <BingoBox
      onClick={onClick}
      style={{
        backgroundColor: isClicked ? "lime" : "khaki",
      }}
    >
      {name}
    </BingoBox>
  );
};

export default Box;
