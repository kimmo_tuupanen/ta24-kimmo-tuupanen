{
  const animals: string[] = [
    "horse",
    "cow",
    "dog",
    "hamster",
    "donkey",
    "cat",
    "parrot",
  ];

  // Create a program that finds us the index of the first animal that has 6 or more letters in its name and prints the index.
  const index = animals.findIndex((animal) => animal.length >= 6);
  console.log(index);

  // Based on the found index, print the value as well.
  console.log(animals.at(index));
}
