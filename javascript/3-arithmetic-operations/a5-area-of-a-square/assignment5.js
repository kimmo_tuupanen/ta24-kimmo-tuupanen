// Create a variable for the length of a side in a square.
let sideLength = 4;

// Calculate and print out the area of a square with the given length of each side.
const area = sideLength * sideLength;
console.log(area + " square meters");

// EXTRA: Calculate the area with exponentiation ** instead of multiplication *.

const expArea = (sideLength **= 2);
console.log(expArea + " square meters");
