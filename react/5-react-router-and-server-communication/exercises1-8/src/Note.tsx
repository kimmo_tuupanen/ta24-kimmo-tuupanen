interface NoteProps {
  noteObj: {
    id: number;
    content: string;
    date: string;
    important: boolean;
  };
  toggleImportance: (id: number) => void;
}

function Note({ noteObj, toggleImportance }: NoteProps) {
  const { id, content, important } = noteObj;
  return (
    <div className="note-container">
      <p style={important ? { color: "red" } : { color: "green" }}>{content}</p>
      <button className="btn btn-note" onClick={() => toggleImportance(id)}>
        Important?
      </button>
    </div>
  );
}

export default Note;
