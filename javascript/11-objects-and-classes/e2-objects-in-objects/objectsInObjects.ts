// 1

interface Student {
  name: string;
  credits: number;
  gender?: string | undefined;
  courseGrades: any;
}

const student: Student = {
  name: "Kim",
  credits: 55,
  courseGrades: {
    "Advanced CSS and Sass": 4,
    "JavaScript Basics": 3,
    "Ultimate React Course": 5,
  },
};
console.log(student);

// 2
student.courseGrades["Program Design"] = 3;
console.log(student);

// 3
student.courseGrades["JavaScript Basics"] = 4;
console.log(student);
