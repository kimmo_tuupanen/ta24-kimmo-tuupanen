import axios from "axios";

async function getUniversities() {
  try {
    const response = await axios.get(
      "http://universities.hipolabs.com/search?country=Finland"
    );
    const data = response.data;
    console.log(data);
  } catch (error) {
    throw new Error("Failed to fetch!");
  }
}

getUniversities();
