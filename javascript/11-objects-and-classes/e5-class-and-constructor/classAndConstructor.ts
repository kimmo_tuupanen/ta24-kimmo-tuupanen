class Rentangle {
  constructor(public width: number, public height: number) {}
}

const rentangleOne = new Rentangle(5, 3);
const rentangleTwo = new Rentangle(7, 4);

console.log(rentangleOne);
console.log(rentangleTwo);
