import styled from "styled-components";
import Person from "./Person";

interface Person {
  name: string;
  age: number;
}

interface FamilyProps {
  people: Person[];
}

const Title = styled.h3`
  font-size: 1.6rem;
  background-color: #353535;
  border: 2px solid #505050;
  padding: 1rem;
  border-radius: 8px;
`;

function Family({ people }: FamilyProps) {
  return (
    <>
      <Title>My family members</Title>
      {people.map((person) => (
        <Person key={person.name} name={person.name} age={person.age} />
      ))}
    </>
  );
}

export default Family;
