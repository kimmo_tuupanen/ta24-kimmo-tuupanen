const topSection = document.querySelector(".top");
const leftSection = document.querySelector(".left");
const rightSection = document.querySelector(".right");

function onMouseEnter(section) {
  const bg = section.style.backgroundColor;
  section.addEventListener("mouseenter", () => {
    section.style.backgroundColor = "yellow";
  });
  section.addEventListener("mouseleave", () => {
    section.style.backgroundColor = `${bg}`;
  });
}
