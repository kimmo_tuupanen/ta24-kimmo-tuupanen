const numbers = [5, 34, 2, 89, 53, 7, 25];

// 1)
let total = 0;
numbers.forEach((num) => (total += num));
console.log(total);

// 2)
const reduceMethod = numbers.reduce((acc, cur) => acc + cur);
console.log(reduceMethod);
