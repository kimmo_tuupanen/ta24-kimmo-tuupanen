import { Year } from "./Year";

export const HelloWorld = ({ name, year }) => {
  return (
    <>
      <h1>Hello {name}!</h1>
      <Year year={year} />
    </>
  );
};
