import { useContext, useState } from "react";
import { TodoContext } from "./NoteContext";

type TodoNoteProps = {
  todo: { id: string; text: string; complete: boolean };
};

function TodoNote({ todo }: TodoNoteProps) {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [inputText, setInputText] = useState(todo.text);
  const { editTodo, toggleCompletion, removeTodo } = useContext(TodoContext);
  const { id, text, complete } = todo;

  function handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    setInputText(e.target.value);
  }

  return (
    <>
      <div
        className="todoNote"
        style={
          complete
            ? { backgroundColor: "aquamarine" }
            : { backgroundColor: "darkseagreen" }
        }
      >
        <input
          type="checkbox"
          checked={complete}
          onChange={() => toggleCompletion(id)}
        />
        {!editMode ? (
          <p>{text}</p>
        ) : (
          <input
            className="input"
            type="text"
            placeholder={inputText}
            onChange={handleInputChange}
          />
        )}

        <button className="btn-delete" onClick={() => removeTodo(id)}>
          X
        </button>
        <div style={{ width: "100%" }}>
          {!editMode ? (
            <button className="btn" onClick={() => setEditMode(true)}>
              Edit
            </button>
          ) : (
            <button
              className="btn"
              onClick={() => {
                editTodo(id, inputText);
                setEditMode(false);
              }}
            >
              Save
            </button>
          )}
        </div>
      </div>
    </>
  );
}

export default TodoNote;
