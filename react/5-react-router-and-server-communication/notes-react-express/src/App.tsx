import "./App.css";
import { useContext, useState } from "react";
import { TodoContext } from "./NoteContext";
import TodoNote from "./TodoNote";
import InputForm from "./InputForm";
import SearchNote from "./SearchNote";

function App() {
  const { todos } = useContext(TodoContext);
  const [searchQuery, setSearchQuery] = useState("");

  const todosToShow = todos.filter((todo) =>
    todo.text.toLowerCase().includes(searchQuery)
  );

  return (
    <div className="container">
      <InputForm />
      <SearchNote setSearchQuery={setSearchQuery} />
      {todosToShow.map((todo) => (
        <TodoNote key={todo.id} todo={todo} />
      ))}
    </div>
  );
}

export default App;
