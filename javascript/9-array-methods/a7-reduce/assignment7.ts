// 1
function total(arr: number[]) {
  return arr.reduce((acc, cur) => acc + cur);
}

// console.log(total([1, 2, 3])); // 6

// 2
function stringConcat(arr: number[]) {
  return arr.reduce((acc, cur) => acc + cur.toString(), "");
}

// console.log(stringConcat([1, 2, 3])); // "123"

// 3
function totalVotes(arr: Voter[]): number {
  const votes = arr.filter((voter) => voter.voted === true).length;
  return votes;
}

interface Voter {
  name: string;
  age: number;
  voted: boolean;
}

var voters = [
  { name: "Bob", age: 30, voted: true },
  { name: "Jake", age: 32, voted: true },
  { name: "Kate", age: 25, voted: false },
  { name: "Sam", age: 20, voted: false },
  { name: "Phil", age: 21, voted: true },
  { name: "Ed", age: 55, voted: true },
  { name: "Tami", age: 54, voted: true },
  { name: "Mary", age: 31, voted: false },
  { name: "Becky", age: 43, voted: false },
  { name: "Joey", age: 41, voted: true },
  { name: "Jeff", age: 30, voted: true },
  { name: "Zack", age: 19, voted: false },
];
// console.log(totalVotes(voters)); // 7

// 4
function shoppingSpree(arr: Wishlist[]) {
  return arr.reduce((acc, cur) => acc + cur.price, 0);
}

interface Wishlist {
  title: string;
  price: number;
}

var wishlist: Wishlist[] = [
  { title: "Tesla Model S", price: 90000 },
  { title: "4 carat diamond ring", price: 45000 },
  { title: "Fancy hacky Sack", price: 5 },
  { title: "Gold fidgit spinner", price: 2000 },
  { title: "A second Tesla Model S", price: 90000 },
];

// console.log(shoppingSpree(wishlist)); // 227005

// 5
function flatten(arr: VariousArrTypes) {
  // @ts-ignore
  return arr.reduce((acc, cur) => acc.concat(cur), []);
}

type VariousArrTypes = (string[] | boolean[] | number[])[];

var arrays: VariousArrTypes = [["1", "2", "3"], [true], [4, 5, 6]];

// console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];

// 6
var voters: Voter[] = [
  { name: "Bob", age: 30, voted: true },
  { name: "Jake", age: 32, voted: true },
  { name: "Kate", age: 25, voted: false },
  { name: "Sam", age: 20, voted: false },
  { name: "Phil", age: 21, voted: true },
  { name: "Ed", age: 55, voted: true },
  { name: "Tami", age: 54, voted: true },
  { name: "Mary", age: 31, voted: false },
  { name: "Becky", age: 43, voted: false },
  { name: "Joey", age: 41, voted: true },
  { name: "Jeff", age: 30, voted: true },
  { name: "Zack", age: 19, voted: false },
];

function voterResults(arr: Voter[]) {
  const numYoungPeople = arr.filter((person) => person.age <= 25).length;
  const numYoungVotes = arr.filter(
    (person) => person.age <= 25 && person.voted === true
  ).length;
  const numMidPeople = arr.filter(
    (person) => person.age >= 26 && person.age <= 35
  ).length;
  const numMidVotes = arr.filter(
    (person) => person.age >= 26 && person.age <= 35 && person.voted === true
  ).length;
  const numOldPeople = arr.filter(
    (person) => person.age >= 36 && person.age <= 55
  ).length;
  const numOldVotes = arr.filter(
    (person) => person.age >= 36 && person.age <= 55 && person.voted === true
  ).length;

  return {
    numYoungVotes: numYoungVotes,
    numYoungPeople: numYoungPeople,
    numMidVotes: numMidVotes,
    numMidPeople: numMidPeople,
    numOldVotes: numOldVotes,
    numOldPeople: numOldPeople,
  };
}

console.log(voterResults(voters)); // Returned value shown below:
