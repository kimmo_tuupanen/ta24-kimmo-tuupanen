class Robot {
  constructor(public x: number, public y: number) {}
  handleMessage(msg: string) {
    if (msg === "moveNorth") {
      this.x++;
    } else if (msg === "moveEast") {
      this.y++;
    } else if (msg === "moveSouth") {
      this.x--;
    } else if (msg === "moveWest") {
      this.y--;
    }
  }
}

class FlexibleRobot extends Robot {
  handleMessage(msg: string) {
    if (msg === "moveNE") {
      this.x++;
      this.y++;
    } else if (msg === "moveSE") {
      this.x++;
      this.y--;
    } else if (msg === "moveSW") {
      this.x--;
      this.y--;
    } else if (msg === "moveNW") {
      this.x--;
      this.y++;
    } else {
      super.handleMessage(msg);
    }
  }
}

const myRobot = new FlexibleRobot(0, 0);
myRobot.handleMessage("moveNE");
myRobot.handleMessage("moveNE");
myRobot.handleMessage("moveNorth");
myRobot.handleMessage("moveNorth");

console.log(myRobot); // (x = 4, y = 2)
