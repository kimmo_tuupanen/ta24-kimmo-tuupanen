import { useState } from "react";
import styled from "styled-components";

const Button = styled.button`
  font-size: 1.6rem;
  background-color: #353535;
  border: 2px solid #505050;
  padding: 1rem 1.5rem;
  border-radius: 8px;
  width: fit-content;
`;

type CounterButtonProps = {
  setSum: React.Dispatch<React.SetStateAction<number>>;
};

function CounterButton({ setSum }: CounterButtonProps) {
  const [count, setCount] = useState(0);
  return (
    <Button
      onClick={() => {
        setCount((prevCount) => prevCount + 1);
        setSum((prevSum) => prevSum + 1);
      }}
    >
      {count}
    </Button>
  );
}

export default CounterButton;
