// randomly generated N = 10 length array 0 <= A[N] <= 99
const randomArray: number[] = Array.from({ length: 10 }, () =>
  Math.floor(Math.random() * 100)
);
console.log(randomArray);

const arraySorted: number[] = randomArray.sort((a, b) => a - b);
console.log(arraySorted);
