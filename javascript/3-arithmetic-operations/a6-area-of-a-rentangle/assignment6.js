// Create two variables: width and length.
const width = 6;
const length = 8;

// Like in the last task, calculate and print out the area. However, instead of a square, we're now dealing with a rectangle.
const area = width * length;
console.log(area);

// Each variable represents the length of two opposing sides of the rectangle.

// EXTRA: In addition to a rectangle, calculate the area of a triangle, with the variables representing the length of the triangle's cathetuses.
const triangleArea = (width * length) / 2;
console.log(triangleArea);
