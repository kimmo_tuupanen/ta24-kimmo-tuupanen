import { jokes as jokesArray } from "./jokes.js";

const randomButton = document.getElementById("random");
const nerdyButton = document.getElementById("nerdy");
const allButton = document.getElementById("all");
const deleteButton = document.getElementById("delete");

randomButton.addEventListener("click", () => {
  createHTML(getJoke("random"));
  deleteButton.disabled = true;
});

nerdyButton.addEventListener("click", () => {
  createHTML(getJoke("nerdy"));
  deleteButton.disabled = true;
});

allButton.addEventListener("click", () => {
  createHTML(getJoke("all"));
  deleteButton.disabled = false;
});

deleteButton.addEventListener("click", () => {
  createHTML(eraseJoke());
});

function getJoke(param) {
  if (param === "random") {
    const randomIndex = Math.floor(Math.random() * jokesArray.length);
    return jokesArray[randomIndex].joke;
  } else if (param === "nerdy") {
    const nerdys = jokesArray.filter((jokeObj) => {
      return jokeObj.categories.at(0) === "nerdy";
    });
    const randomIndex = Math.floor(Math.random() * nerdys.length);
    return nerdys[randomIndex].joke;
  } else {
    return jokesArray.map((jokeObj) => jokeObj.joke); // return array of all jokes
  }
}

function eraseJoke() {
  jokesArray.splice(0, 1);
  return jokesArray.map((jokeObj) => jokeObj.joke); // return array of the remaining jokes
}

// daddy's little helper function to insert HTML without repetitive code
function createHTML(element) {
  let main = "";

  if (typeof element === "string") {
    main = `
        <div class="content">
          <p>${element}</p>
        </div>
      `;
  } else if (Array.isArray(element)) {
    main = `
        <div class="content">
        <p class="number-of-jokes">Number of jokes: ${element.length}</p>
        <ul>
          ${element
    .map(
      (joke) => `
              <li class="list-of-jokes">${joke}</li>`
    )
    .join("")}        
              </ul>
        </div>
      `;
  }
  document.querySelector(".container").innerHTML = main;
}
