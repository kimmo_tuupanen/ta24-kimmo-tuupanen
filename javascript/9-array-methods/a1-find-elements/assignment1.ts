const listOfNumbers = [8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50];

// a)
for (const num of listOfNumbers) {
  if (num <= 20) continue;
  else console.log(num);
  break;
}

// b)
console.log(listOfNumbers.find((num) => num >= 20));

// c)
const numIndex = listOfNumbers.findIndex((num) => num >= 20);
console.log(numIndex);

// d)
listOfNumbers.splice(numIndex + 1);

console.log(listOfNumbers);
