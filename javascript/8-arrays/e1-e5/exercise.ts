
// 1
const wordArray: string[] = ["the", "quick", "brown", "fox"];
console.log(wordArray);
console.log(wordArray[1], wordArray[2]);

// 2
wordArray[2] = "gray";

// 3 push, unshift
wordArray.push("over", "lazy", "dog");
wordArray.unshift("pangram:")

// 4 splice
wordArray.splice(5, 0, "jumps")
wordArray.splice(7, 0, "the")

// 5 shift, pop
wordArray.shift();
wordArray.pop();
wordArray.splice(2, 1);
const removed = wordArray.splice(3);

console.log(removed);
console.log(wordArray);
