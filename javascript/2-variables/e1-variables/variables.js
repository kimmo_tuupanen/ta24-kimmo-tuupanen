let color = "yellow";
let number = 2;
let whatIf; // returns undefined
console.log(color, number, whatIf);

color = "red";
number = 4;
console.log(color, number);

// What happens when you try to change the value of the constant variable?
// TypeError: Assignment to constant variable.
