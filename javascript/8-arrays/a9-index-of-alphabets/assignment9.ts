const charIndex: any = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
  e: 5,
  f: 6,
  g: 7,
  h: 8,
  i: 9,
  j: 10,
  k: 11,
  l: 12,
  m: 13,
  n: 14,
  o: 15,
  p: 16,
  q: 17,
  r: 18,
  s: 19,
  t: 20,
  u: 21,
  v: 21,
  w: 22,
  x: 23,
  y: 24,
  z: 25,
};

function charIndexString(str: string) {
  let indexString = "";

  for (let i = 0; i < str.length; i++) {
    const charIndexOfLetter = charIndex[str.charAt(i)];
    indexString += charIndexOfLetter;
    console.log(charIndexOfLetter);
  }
  return indexString;
}

console.log(charIndexString("bead"));
console.log(charIndexString("rose"));
