import styled from "styled-components";

const ButtonContainer = styled.div`
  position: relative;
`;

const Button = styled.button`
  font-size: 1.6rem;
  background-color: #d4d4d4;
  border: 2px solid #929292;
  padding: 1rem 1.5rem;
  border-radius: 8px;
  width: fit-content;
`;

const DeleteButton = styled.button`
  font-size: 1rem;
  position: absolute;
  z-index: 2;
  top: -10px;
  right: -13px;
  background-color: tomato;
  border: 1px solid #929292;
  padding: 4px 8px;
  border-radius: 8px;
  cursor: pointer;
`;

type CounterButtonProps = {
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
  onDeleteCounter: () => void;
  count: number;
};

function CounterButton({
  onClick,
  onDeleteCounter,
  count,
}: CounterButtonProps) {
  return (
    <ButtonContainer>
      <Button onClick={onClick}>{count}</Button>
      <DeleteButton onClick={onDeleteCounter}>X</DeleteButton>
    </ButtonContainer>
  );
}

export default CounterButton;
