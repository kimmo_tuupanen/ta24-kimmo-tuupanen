// A store is rising the price of a product and is not sure of the result.

// Create a program that calculates the result for them: have two variables, price and increase. Based on these, calculate the final price into a variable named result.

const price = 5.5;
const increase = 1.5;
const result = price + increase;

// Print the values of all of these variables on their own lines, with fitting explanations. For example, "Original price: 6.5".

console.log(`Original price: ${price}€`);
console.log(`Increase: ${increase}€`);
console.log(`Increased price: ${result}€`);

// EXTRA: Print the whole calculation in a single line, like price + increase = result

console.log(
  `Original price ${price}€ + increase ${increase}€ equals ${result}€.`
);
