const numArray: number[] = [1, 4, 6, 7, 9, 13, 19, 22];

function sortNumberArray(array: number[]) {
  let isSorted = false;

  while (!isSorted) {
    isSorted = true;

    for (let [index, num] of array.entries()) {
      if (num < index - 1) {
        let temp = num;
        num = array[index - 1];
        array[index - 1] = temp;
        isSorted = false;
      }
    }
    // for (let i = 1; i < arr.length; i++) {
    //   if (arr[i] < arr[i - 1]) {  // [1] < [0] ?
    //     let temp = arr[i];        // temp = [1]
    //     arr[i] = arr[i - 1];      // [1] = [0]
    //     arr[i - 1] = temp;        // [0] = [1]
    //     isSorted = false;
    //   }
    // }
  }
}

sortNumberArray(numArray);
console.log(numArray); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]
