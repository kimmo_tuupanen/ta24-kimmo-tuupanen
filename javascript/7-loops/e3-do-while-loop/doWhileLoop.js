let x = 5;
let sum = 0;
let add = 3;
let numberSequence = "";

do {
    sum = sum + add;
    numberSequence += `${sum}, `;
    x--; 
} while (x >= 1);


console.log(numberSequence.slice(0, -2)); // 3, 6, 9, 12, 15

