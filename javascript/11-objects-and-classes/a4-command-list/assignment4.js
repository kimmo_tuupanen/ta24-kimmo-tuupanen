const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

let x = 0;
let y = 0;

const commandHandlers = {
  N: () => y++,
  E: () => x++,
  S: () => y--,
  W: () => x--,
};

for (const command of commandList) {
  if (command === "B") break;
  if (command === "C") continue;
  const func = commandHandlers[command];
  func();
}

console.log(`Final position of robot x = ${x}, y = ${y}`);
