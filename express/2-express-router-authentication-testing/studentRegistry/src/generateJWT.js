import "dotenv/config";
import jwt from "jsonwebtoken";

const payload = { username: "snoopDoggyDogg", email: "snoop@yahoo.com" };
// const secret = process.env.SECRET;

const secret = "mySecret";
const options = { expiresIn: "1h" };

const token = jwt.sign(payload, secret, options);

console.log(token);
