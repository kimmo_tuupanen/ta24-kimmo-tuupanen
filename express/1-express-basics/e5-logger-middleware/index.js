import express, { json } from "express";
import { logger } from "./middleware.js";

const server = express();
const port = 3500;

let students = [];

server.use(express.json());
server.use(logger);

//** POST *//

server.post("/student", (req, res) => {
  const { id, name, email } = req.body;

  if (!id || !name || !email) {
    return res.status(400).json({ message: "Missing or invalid parameters" });
  }
  students.push({ id, name, email });

  return res.status(201).end();
});

//** PUT *//

server.put("/student/:id", (req, res) => {
  const paramsId = req.params.id;
  const { name, email } = req.body;
  if (!name && !email)
    return res.status(400).json({ message: "Name or email must be provided" });

  const student = students.find((student) => {
    return student.id === paramsId;
  });

  if (!student) {
    return res.status(404).json({ error: "Student not found" });
  }

  if (name) student.name = name;
  if (email) student.email = email;

  res.status(204).send();
});

//** DELETE *//

server.delete("/student/:id", (req, res) => {
  const paramsId = req.params.id;

  const index = students.findIndex((student) => student.id === paramsId);
  if (index === -1) {
    return res.status(404).json({ error: "Student not found" });
  }

  students.splice(index, 1);
  res.status(204).send();
});

//** GET *//

server.get("/students", (req, res) => {
  res.json(students.map((student) => student.id));
});

server.get("/student/:id", (req, res) => {
  const paramsId = req.params.id;
  const student = students.find((student) => {
    return student.id === paramsId;
  });

  if (!student) {
    return res.status(404).json({ message: "Student not found" });
  }
  return res.json(student);
});

// 404 error
server.use((req, res) => {
  res.status(404).json({ message: "URL Not Found" });
});

server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
