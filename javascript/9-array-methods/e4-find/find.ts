{
  const animals: string[] = [
    "horse",
    "cow",
    "dog",
    "hamster",
    "donkey",
    "cat",
    "parrot",
  ];

  // Create a program that finds us the first animal that ends in letter ‘t’.

  const endsWithT = animals.find((animal) => animal.endsWith("t"));
  console.log(endsWithT);

  // EXTRA: Find the first animal that ends in letter ‘y’ and starts with letter ‘d’

  const extra = animals.find(
    (animal) => animal.startsWith("d") && animal.endsWith("y")
  );
  console.log(extra);
}
