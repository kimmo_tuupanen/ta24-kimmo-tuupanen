{
  class Shape {
    constructor(public width: number, public height: number) {}

    getArea(): number {
      return 0;
    }
  }

  class Rectangle extends Shape {
    getArea(): number {
      return this.width * this.height;
    }
  }

  class Ellipse extends Shape {
    getArea(): number {
      const pi = 3.14159;
      return (((pi * this.width) / 2) * this.height) / 2;
    }
  }

  const shape = new Shape(3, 3);
  const rectangle = new Rectangle(6, 3);
  const ellipse = new Ellipse(2, 3);

  console.log(rectangle);
  console.log(rectangle.getArea());
  console.log(ellipse.getArea());
}
