// a)

function calculateTotalScore(grades: string) {
  const scores = { S: 8, A: 6, B: 4, C: 3, D: 2, F: 0 };

  let totalScore = 0;
  for (const grade of grades) {
    // @ts-ignore
    totalScore += scores[grade];
  }
  return totalScore;
}

const totalScore = calculateTotalScore("DFCBDABSB");
// console.log(totalScore); // prints 33

// b)

function calculateAverageScore(grades: string): number {
  const averageScore = calculateTotalScore(grades) / grades.length;
  return averageScore;
}

const averageScore = calculateAverageScore("DFCBDABSB");
// console.log(averageScore); // prints 3.6666666666666665

// c)
// Use Array.map to convert this array into an array of average scores of the sequences. Print the resulting array.

const gradeArray = ["AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC"];

const averageScores = gradeArray.map(calculateAverageScore);
console.log(averageScores);
