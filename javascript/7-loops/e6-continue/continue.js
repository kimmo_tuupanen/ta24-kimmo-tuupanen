let factorial = 1; 

for(let i = 1; i <= 10; i++) {
    if(i % 3 === 0) continue;

    factorial = factorial * i;  
};

console.log(`The factorial is ${factorial}`);  