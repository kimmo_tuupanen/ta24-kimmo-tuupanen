// a)

function sum(limit) {
  let sum = 0;
  while (limit > 0) {
    sum += limit;
    limit--;
  }
  return sum;
}

console.log(sum(10));

// b)

new Promise((resolve) => {
  resolve(sum(50000));
}).then((value) => {
  console.log(value);
});

// c)

const myPromise = new Promise((resolve) => {
  setTimeout(() => {
    resolve(sum(666));
    console.log("Resolved after 2 secs!");
  }, 2000);
});

async function printSum() {
  const sum = await myPromise;
  console.log(sum);
}

printSum();

// d)

function createDelayedCalculation(limit, milliseconds) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(sum(limit));
    }, milliseconds);
  });
}

// Prints 200000010000000 after a delay of 2 seconds
createDelayedCalculation(20000000, 2000).then((result) => console.log(result));

// Prints 1250025000 after a delay of 0.5 seconds
createDelayedCalculation(50000, 500).then((result) => console.log(result));
