function crash(n: number) {
  console.log(n);
  return crash(n + 1);
}

crash(1);
// 9053
// RangeError: Maximum call stack size exceeded
