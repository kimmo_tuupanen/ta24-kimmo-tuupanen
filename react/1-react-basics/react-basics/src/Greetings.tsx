interface Props {
  name: string;
  age: number;
}

export const Greetings = ({ name, age }: Props) => {
  return (
    <p>
      My name is {name} and I am {age} years old!
    </p>
  );
};
