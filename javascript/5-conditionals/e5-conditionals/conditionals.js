// 5a
let playerCount = 4;
if(playerCount === 4) {}

// 5b
let isStressed = false;
let hasIceCream = true;
if(isStressed && hasIceCream) {};

// 5c
let isSunny = true;
let isRaining = true;
let temperature = 15; 
if(isSunny && !isRaining && temperature >= 20) {}

// 5d
let friend = "Suzy";
let day = "Tuesday";

if(friend === "Dan" && day === "Tuesday" || friend === "Suzy" && day === "Tuesday") {}
