const wordArray = ["The", "quick", "silver", "wolf"];

function sentencify(arr: string[], index: number): string {
  if (index >= arr.length - 1) {
    return `${arr[index]}!`;
  }
  return arr[index] + " " + sentencify(arr, index + 1);
}

console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"
