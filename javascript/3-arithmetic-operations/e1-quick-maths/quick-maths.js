// Create a program and assign two numbers to variables a and b.
const a = 7;
const b = 5;

// Calculate the sum, difference, fraction and product of these numbers.
const sum = a + b;
const difference = a - b;
const fraction = a / b;
const product = a * b;
console.log(sum, difference, fraction, product);

// Assign third number to variable c and try out calculating a + b * c.
const c = 3;
let myNumber = a + b * c;
console.log(myNumber); // 22

// How does the result change if you add parentheses to the calculation
// (a + b) * c
myNumber = (a + b) * c;
console.log(myNumber); // 36

// a + (b * c) doesn't change a thing, result is same as
myNumber = a + b * c;
console.log(myNumber); // 22
