import { useState } from "react";

// E1
const Input = () => {
  const [text, setText] = useState("");
  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  };

  return (
    <>
      <p>{text}</p>
      <input type="text" onChange={onInputChange}></input>
    </>
  );
};

export default Input;
