import "dotenv/config";
import jwt from "jsonwebtoken";

const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidXNlcm5hbWUiOiJKb2huIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.zmGEYNcb3J41P3aw8HdIJ1mG7tgDRjV1mU4JQnoSIUg";
const secret = "mySecretofSecrets";

// Verify the token
jwt.verify(token, secret, (err, decoded) => {
  if (err) {
    console.error("Error verifying token:", err.message);
    process.exit(1); // Exit with error status
  } else {
    console.log("Verified JWT:");
    console.log(decoded);
    process.exit(0); // Exit with success status
  }
});
