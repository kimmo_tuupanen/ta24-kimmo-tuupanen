// Create a program that has a variable representing the number of a month. 
//(1 = January, 2 = February and so on)

// The program should print how many days there are in the given month. Do it using an if-else if... structure.

let month = 1;
let days;

if(month === 1) {
    days = 31;
} else if(month === 2) {
    days = 28;
} else if(month === 3) {
    days = 31;
} else if(month === 4) {
    days = 30;
} else if(month === 5) {
    days = 31;
} else if(month === 6) {
    days = 30;
} else if(month === 7) {
    days = 31;
} else if(month === 8) {
    days = 30;
} else if(month === 9) {
    days = 31;
} else if(month === 10) {
    days = 30;
} else if(month === 11) {
    days = 31;
} else if(month === 12) {
    days = 30;
}

console.log(`There are ${days} days in month ${month}.`)

