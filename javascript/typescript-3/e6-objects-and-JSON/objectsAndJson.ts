interface Library {
  author: string;
  title: string;
  readingStatus: boolean;
  id: number;
}

const library: Library[] = [
  {
    author: "David Wallace",
    title: "Infinite Jest",
    readingStatus: false,
    id: 1,
  },
  {
    author: "Douglas Hofstadter",
    title: "Gödel, Escher, Bach",
    readingStatus: true,
    id: 2,
  },
  {
    author: "Harper Lee",
    title: "To Kill A Mockingbird",
    readingStatus: false,
    id: 3,
  },
];

function getBook(id: number) {
  const book = library.find((book) => book.id === id);
  return book?.title;
}

function printBookData(id: number) {
  const book = library.find((book) => book.id === id);
  return book;
}

function printReadingStatus(author: string, title: string) {
  const book = library.find((book) => {
    return book.author === author && book.title === title;
  });
  return book?.readingStatus;
}

function addNewBook(author: string, title: string) {
  library.push({
    author,
    title,
    readingStatus: false,
    id: library.length + 1,
  });
}

function readBook(id: number) {
  return library.find((book) => {
    book.id === id;
    book.readingStatus = true;
  });
}

function saveToJSON(object: unknown) {
  const myJSON = JSON.stringify(object);
  return myJSON;
}

function loadFromJSON(json: string) {
  const data = JSON.parse(json);
  return data;
}

console.log(`Book title: ${getBook(1)}`);
console.log(printBookData(1));
console.log(printReadingStatus("David Wallace", "Infinite Jest"));
addNewBook("Holly Jackson", "Kiltti tyttö, kohta kuollut");
readBook(1);
console.log(saveToJSON(library));
