// 1
function modify(arr) {
    arr.splice(0, 1, "twenty-two")
}

const myArray = [22, 33, 44];
console.log(myArray);

modify(myArray);
console.log(myArray);

// 2
function objectModifier(obj) {
    obj.color = "yellow";
    return obj;
}

const circle = {
    radius: 90,
    
}

console.log(circle);
objectModifier(circle);
console.log(circle);
