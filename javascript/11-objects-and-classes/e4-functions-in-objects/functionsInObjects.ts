type BSC = {
  name: string;
  cache: string;
  clockSpeed: number;
  overClock: () => void;
  savePower: () => void;
};

const bSuperCalc: BSC = {
  name: "Buutti SuperCalculator 6000",
  cache: "96GB",
  clockSpeed: 9001.0,
  overClock: function () {
    this.clockSpeed += 500;
  },
  savePower: function () {
    if (this.clockSpeed > 2000) {
      this.clockSpeed -= 2000;
    } else {
      this.clockSpeed /= 2;
    }
  },
};
bSuperCalc.overClock();
bSuperCalc.savePower();
bSuperCalc.savePower();
bSuperCalc.savePower();
bSuperCalc.savePower();
bSuperCalc.savePower();
bSuperCalc.overClock();
console.log(bSuperCalc);
