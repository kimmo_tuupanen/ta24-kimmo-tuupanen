// a)

async function waitFor(milliseconds) {
  const waiting = new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, milliseconds);
  });
  await waiting;
}

waitFor(3000);

// b)

async function countSeconds() {
  let count = 0;
  while (count <= 10) {
    console.log(count);
    count++;
    await waitFor(1000);
  }
}

countSeconds();
