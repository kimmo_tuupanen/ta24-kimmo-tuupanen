import { ReactNode, createContext, useEffect, useState } from "react";
import noteService from "./noteService";
import { v4 as uuidv4 } from "uuid";

interface Todo {
  id: string;
  text: string;
  complete: boolean;
}

interface TodoContextType {
  todos: Todo[];
  addTodo: (text: string) => void;
  toggleCompletion: (id: string) => void;
  removeTodo: (id: string) => void;
  editTodo: (id: string, newText: string) => void;
}

const defaultValue: TodoContextType = {
  todos: [],
  addTodo: () => {},
  toggleCompletion: () => {},
  removeTodo: () => {},
  editTodo: () => {}, // Placeholder function for editing a todo
};

export const TodoContext = createContext<TodoContextType>(defaultValue);

export const TodoProvider = ({ children }: { children: ReactNode }) => {
  const [todos, setTodos] = useState<Todo[]>([]);

  useEffect(() => {
    noteService.getAll().then((initialTodos) => setTodos(initialTodos));
  }, []);

  function toggleCompletion(id: string) {
    const note = todos.find((note) => note.id === id);
    if (!note) return;
    const changedNote = {
      ...note,
      complete: !note.complete,
    };
    noteService.update({ id, newObject: changedNote }).then((returnedNote) => {
      setTodos(todos.map((todo) => (todo.id !== id ? todo : returnedNote)));
    });
  }

  function editTodo(id: string, newText: string) {
    const note = todos.find((note) => note.id === id);
    if (!note) return;
    const updatedNote = {
      ...note,
      text: newText,
    };
    noteService
      .update({ id, newObject: updatedNote })
      .then((returnedNote) => {
        setTodos(todos.map((todo) => (todo.id === id ? returnedNote : todo)));
      })
      .catch((error) => {
        console.error("Failed to update the note:", error);
      });
  }

  function removeTodo(id: string) {
    noteService
      .deleteItem({ id })
      .then((returnedTodos) => {
        setTodos(returnedTodos);
      })
      .catch((error) => {
        console.error("Failed to update the note:", error);
      });
  }

  function addTodo(text: string) {
    const newNote = { id: uuidv4(), text, complete: false };
    noteService.create({ newObject: newNote }).then((returnedNote) => {
      setTodos([...todos, returnedNote]);
    });
  }

  return (
    <TodoContext.Provider
      value={{ todos, addTodo, toggleCompletion, removeTodo, editTodo }}
    >
      {children}
    </TodoContext.Provider>
  );
};
