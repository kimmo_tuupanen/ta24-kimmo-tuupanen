// 1
const personalData = {
    name: "Kimmo",
    age: 50,
    pineappleOnPizza: false,
    
}

// 1.a/b/c
personalData.hobbycar = "Corvette C3";
personalData.name = "Kimmo Tuupanen";
console.log(typeof(personalData.name));
console.log(typeof(personalData.age));
console.log(typeof(personalData.pineappleOnPizza));
console.log(typeof(personalData.hobbycar));

// 2
console.log(personalData); // objects printed with braces

// 3
console.log(typeof(personalData));
