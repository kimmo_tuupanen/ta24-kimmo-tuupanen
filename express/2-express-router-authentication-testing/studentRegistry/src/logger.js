export const logger = (req, res, next) => {
  const requestTime = new Date().toISOString();
  const requestMethod = req.method;
  const requestUrl = req.url;

  const logEntry = `requested @ [${requestTime}], method: ${requestMethod}, url: ${requestUrl}`;

  // server will parse incoming JSON body
  if (req.body && Object.keys(req.body).length) {
    console.log(`${logEntry}, body: ${JSON.stringify(req.body)}`);
  } else {
    console.log(logEntry);
  }

  next();
};
