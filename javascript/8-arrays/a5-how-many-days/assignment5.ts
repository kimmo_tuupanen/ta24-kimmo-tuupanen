// Create a program that has a variable representing the number of a month.
//(1 = January, 2 = February and so on)
// The program should print how many days there are in the given month. Do it using an array and indexing.

const howManyDays = (month: number) => {
  const daysPerMonth: number[] = [
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
  ];
  return daysPerMonth[month - 1];
};

console.log(howManyDays(2));
