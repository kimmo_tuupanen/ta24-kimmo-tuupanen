// Create variables for distance (kilometers) and speed (km/h), and give them some values. Calculate and print out how many hours it takes to travel the distance at the given speed.

const distance = 120;
const speed = 50;
const minsToTravel = Math.floor((distance / speed) * 60);

function timeConvert(num) {
  // Calculate the number of hours by dividing num by 60 and rounding down
  const hours = Math.floor(num / 60);

  // Calculate the remaining minutes by taking the remainder when dividing num by 60
  const minutes = num % 60;

  return hours + ":" + minutes;
}

console.log(timeConvert(minsToTravel));

// EXTRA: Express the time in hours and minutes instead of only hours. For example, traveling 120 km at 50 km/h would take 2 hours 24 minutes.
