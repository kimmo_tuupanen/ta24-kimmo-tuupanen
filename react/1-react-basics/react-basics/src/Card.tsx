function Card() {
  return (
    <div className="card">
      <img className="contain" src="./r2d2.jpg" alt="R2D2" />
      <h3 className="card-heading">Hello, I am R2D2!</h3>
      <p className="card-text">BeeYoop BeeDeepBoom Weeop DEEpaEEya!</p>
    </div>
  );
}

export default Card;
