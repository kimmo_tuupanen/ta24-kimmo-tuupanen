const countdownToStart = function (time, func) {
  setTimeout(() => {
    func();
  }, time);
};

countdownToStart(0, () => {
  console.log("3    => Wait for one second!");
  countdownToStart(1000, () => {
    console.log("..2  => Wait another second!");
    countdownToStart(1000, () => {
      console.log("..1  => Wait the last one second...");
      countdownToStart(1000, () => {
        console.log("GO!");
      });
    });
  });
});
