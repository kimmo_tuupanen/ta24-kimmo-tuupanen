// const countdown = function (time, func) {
//   setTimeout(() => {
//     func();
//   }, time);
// };

// new Promise((resolve, reject) => {
//   const execute = false;
//   if (execute) {
//     resolve;
//   } else {
//     reject("!");
//   }
// })
//   .then(() => {
//     countdown(0, () => {
//       console.log("3    => Wait for one second!");
//       countdown(1000, () => {
//         console.log("..2  => Wait another second!");
//         countdown(1000, () => {
//           console.log("..1  => Wait the last one second...");
//           countdown(1000, () => {
//             console.log("GO!");
//           });
//         });
//       });
//     });
//   })

//   .catch((val) => {
//     console.log(val);
//   });

function countdown(seconds) {
  return new Promise((resolve, reject) => {
    let count = seconds;

    function tick() {
      if (count > 0) {
        setTimeout(() => {
          console.log(
            `${".".repeat(seconds - count)}${count} => Wait ${
              count === seconds ? "for one" : "another"
            } second!`
          );
          count--;
          tick();
        }, 1000);
      } else {
        setTimeout(() => {
          resolve();
        }, 1000);
      }
    }

    tick();
  });
}

countdown(4)
  .then(() => {
    console.log("GO!");
  })
  .catch((error) => {
    console.error("Countdown failed:", error);
  });
