// a)

class WeatherEvent {
  constructor(public timestamp: string) {}

  getInformation(): string {
    return "";
  }

  print() {
    console.log(`${this.timestamp} ${this.getInformation()}`);
  }
}

// b)

class TemperatureChangeEvent extends WeatherEvent {
  constructor(public timestamp: string, public temperature: number) {
    super(timestamp);
  }
  getInformation(): string {
    return `temperature: ${this.temperature} °C`;
  }
}

// c)

class HumidityChangeEvent extends WeatherEvent {
  constructor(public timestamp: string, public humidity: number) {
    super(timestamp);
  }
  getInformation(): string {
    return `humidity: ${this.humidity} %`;
  }
}

// d)

class WindStrengthChangeEvent extends WeatherEvent {
  constructor(public timestamp: string, public windStrength: number) {
    super(timestamp);
  }
  getInformation(): string {
    return `humidity: ${this.windStrength} %`;
  }
}

const weatherEvents = [];
const date = new Date().toLocaleString();

weatherEvents.push(new TemperatureChangeEvent(date, -6.4));
weatherEvents.push(new HumidityChangeEvent(date, 95));
weatherEvents.push(new WindStrengthChangeEvent(date, 2.2));

weatherEvents.forEach((weatherEvent) => weatherEvent.print());
