type Student = {
  name: string;
  score: number;
};

const students: Student[] = [
  { name: "Sami", score: 24.75 },
  { name: "Heidi", score: 20.25 },
  { name: "Jyrki", score: 27.5 },
  { name: "Helinä", score: 26.0 },
  { name: "Maria", score: 17.0 },
  { name: "Yrjö", score: 14.5 },
  { name: "Yrjözz", score: 13.5 },
];

const getGrades = (studentList: Student[]) => {
  return studentList.map((student) => {
    let grade;
    if (student.score < 14.0) {
      grade = 0;
    } else if (student.score <= 17.0) {
      grade = 1;
    } else if (student.score <= 20.0) {
      grade = 2;
    } else if (student.score <= 23.0) {
      grade = 3;
    } else if (student.score <= 26.0) {
      grade = 4;
    } else {
      grade = 5;
    }
    return { name: student.name, grade: grade };
  });
};

console.log(getGrades(students));
