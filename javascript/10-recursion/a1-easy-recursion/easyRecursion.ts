function F(n: number): number {
  return n <= 2 ? 1 : F(n - 2) * 3 + F(n - 1);
}

console.log(F(17));

// F(0) = 0;
// F(1) = 1;
// F(n) = F(n - 2) * 3 + F(n - 1);
