import express from "express";
const server = express();

let count = 0;

server.use("/counter", (req, res, next) => {
  count++;
  next();
});

server.get("/:counter", (request, response) => {
  count++;
  response.json({ counter: count });

  console.log({ counter: count });
});

server.listen(3000);
