const years = [8, 1500, 1600, 1700, 1982, 2000, 2024, 2026, 2028];

function leapYear(years) {
    years.forEach(yr => {
        if((yr % 4 === 0 && yr % 100 !== 0) || yr % 400 === 0) {       
           return console.log(`${yr} is leap year!`)        
        } else {
            console.log(`${yr} isn't leap year.`)
        }
    });
}

leapYear(years);