// Calculate how many seconds there are in a year. Use variables for days, hours, minutes and seconds in a year. Print out the result.

const days = 365;
const hours = 24;
const mins = 60;
const seconds = 60;

const secondsPerYear = mins * seconds * hours * days;

console.log(secondsPerYear);
