// cannot reassign constant
// const value = 30;
let value = 30;
const multiplier = 1.2;
value = value * multiplier;
console.log(value);
