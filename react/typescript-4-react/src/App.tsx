import MyDog from "./MyDog";

function App() {
  return (
    <>
      <MyDog name="Waldo" age={1} sex="male" />
      <MyDog name="Seppo" age={10} sex="male" />
      <MyDog name="Noomi" age={3} sex="female" />
    </>
  );
}

export default App;
