const animals: string[] = [
  "horse",
  "cow",
  "dog",
  "hamster",
  "donkey",
  "cat",
  "parrot",
];

// Create a program that prints the names of all animals that have the letter ‘e’ in their name.
// a) implement the program using a for…of loop
for (const animal of animals) {
  if (animal.includes("e")) console.log(animal);
}

// b) implement the program using the forEach method
animals.forEach((animal) => {
  if (animal.includes("e")) console.log(animal);
});
