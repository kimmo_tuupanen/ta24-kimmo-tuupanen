// Create a program with two variables, each representing words.
const one = "property";
const two = "placeholder";

// Using a conditional, check which word is longer. If the first word is longer, the program should print "first word is longer than second word", with the placeholders replaced by the actual words.

if(one.length > two.length) {
    console.log(`${one} is longer than ${two}`)
} else if(one.length < two.length) {
    console.log(`${one} is shorter than ${two}`)
} else {
    console.log(`words are equal at length`)
}



