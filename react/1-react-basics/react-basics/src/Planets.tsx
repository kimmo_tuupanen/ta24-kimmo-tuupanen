interface Planet {
  name: string;
  climate: string;
}

interface PlanetsProps {
  planets: Planet[];
}

function Planets({ planets }: PlanetsProps) {
  const planetList = planets.map((planet) => (
    <tr>
      <td>{planet.name}</td>
      <td>{planet.climate}</td>
    </tr>
  ));

  return (
    <div>
      <table>
        <thead>
          <tr>
            <td>Planet name</td>
            <td>Planet climate</td>
          </tr>
        </thead>
        <tbody>{planetList}</tbody>
      </table>
    </div>
  );
}

// const Planets: React.FC<PlanetsProps> = ({ planets }) => {
//   const planetList = planets.map((planet) => (
//     <tr>
//       <td>{planet.name}</td>
//       <td>{planet.climate}</td>
//     </tr>
//   ));

//   return (
//     <div>
//       <table>
//         <thead>
//           <tr>
//             <td>Planet name</td>
//             <td>Planet climate</td>
//           </tr>
//         </thead>
//         <tbody>{planetList}</tbody>
//       </table>
//     </div>
//   );
// };

export default Planets;
