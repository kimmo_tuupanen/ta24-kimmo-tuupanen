import styled from "styled-components";
import Family from "./Family";
import CounterButton from "./Button";
import { useState } from "react";

const people = [
  { name: "Alice", age: 30 },
  { name: "Bob", age: 35 },
  { name: "Charlie", age: 40 },
  { name: "Donna", age: 45 },
];

const Title = styled.h3`
  font-size: 1.6rem;
  padding: 1rem;
  border-radius: 8px;
  color: blanchedalmond;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.5rem;
  align-items: center;
`;

const Sum = styled.span`
  font-size: 1.6rem;
  margin-top: 0.5rem;
  font-weight: 600;
`;

const Button = styled.button`
  font-size: 1.2rem;
  background-color: #353535;
  border: 2px solid #505050;
  padding: 1rem 1.5rem;
  border-radius: 8px;
  width: fit-content;
  margin-top: 1rem;
`;

function App() {
  const [sum, setSum] = useState(0);
  const [show, setShow] = useState(true);
  return (
    <>
      <Title>Exercise 1 & 2: Arrays & props</Title>
      <Family people={people} />

      <Title>Exercise 3: Counter</Title>
      <Container>
        {show && (
          <>
            <CounterButton setSum={setSum} />
            <CounterButton setSum={setSum} />
            <CounterButton setSum={setSum} />
            <Sum>{sum}</Sum>
          </>
        )}
        <Button
          onClick={() => {
            setShow((show) => !show);
          }}
        >
          {show ? "Hide" : "Show"}
        </Button>
      </Container>
    </>
  );
}

export default App;
