import axios from "axios";

interface University {
  "state-province": null;
  country: string;
  domains: string[];
  web_pages: string[];
  alpha_two_code: string;
  name: string;
}

const URL = "http://universities.hipolabs.com/search?country=Finland";

async function getUniversities() {
  try {
    const response = await axios.get(`${URL}`);
    return response.data;
  } catch (error) {
    throw new Error("Failed to fetch!");
  }
}

getUniversities().then((result) => {
  const universities = result.map((element: University) => {
    return element.name;
  });
  console.log(universities);
});
