const myArr = ["8", 45, 345];

const isArray = (arr: unknown[]): boolean => {
  return arr.every((item) => typeof item === "number");
};

console.log(isArray(myArr));
