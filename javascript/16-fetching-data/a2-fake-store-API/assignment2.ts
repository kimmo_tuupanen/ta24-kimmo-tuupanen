import axios from "axios";

const URL = "https://fakestoreapi.com/products";

interface Product {
  id: string;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: {
    rate: number;
    count: number;
  };
}

// a)
async function getFakeStoreProducts() {
  await axios
    .get(`${URL}`)
    .then((response) => {
      const productNames = response.data.map((product: Product) => {
        return product.title;
      });
      console.log(productNames);
    })
    .catch((error) => console.error("Failed to fetch products", error));
}

getFakeStoreProducts();

// b)
async function addFakeStoreProduct(
  name: string,
  price: number,
  description: string,
  category: string
) {
  await axios
    .post(`${URL}`, {
      title: name,
      price,
      description,
      category,
    })
    .then((response) => console.log("New product id:", response.data.id))
    .catch((error) => console.error("Failed to upload your product", error));
}

addFakeStoreProduct(
  "Silicon Power 256GB SSD",
  109,
  "7mm slim design",
  "electronics"
);

// c)
function deleteFakeStoreProduct(id: number) {
  axios
    .delete(`${URL}/${id}`)
    .then((response) => {
      console.log("Deleted:", response.data.title);
    })
    .catch((error) => console.error("Product was not deleted.", error));
}

deleteFakeStoreProduct(6);
