// 1
const fruitArr = ['pineapple', 'orange', 'wine grape', 'banana'];
console.log(fruitArr.length);
// 1.a
fruitArr[1] = 'kiwi';
console.log(fruitArr)

// 2
fruitArr.push('watermelon');
// 2.a
console.log(fruitArr); // new item added last
// 2.b
console.log(fruitArr.length); // 5

// 3
fruitArr.pop();
// 3.a pop() method removes the last element
// 3.b
console.log(fruitArr.length); // 4

// 4
console.log(fruitArr);
// 4.a elements are inside square brackets

// 5
console.log(typeof(fruitArr));