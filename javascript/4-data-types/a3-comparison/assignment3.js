const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);

// 3 a) 
false

// 3 b) 
typeof(isFirstPersonOlder); // boolean

// 3 c)
// You are teaching students of two classes on a course.
// Students of the first class got grades of 9, 6 and 9.
// Students of the second class got grades of 7, 10, and 5.
const classGrades1 = [9, 6, 9];
const classGrades2 = [7, 10, 5];

// Create a program that calculates the average grade of both classes
const avg = (grades) => grades.reduce((acc, cur) => {
    return acc + cur / grades.length;
}, 0);

const avgClassGrades1 = avg(classGrades1);
const avgClassGrades2 = avg(classGrades2);

// prints whether the first class got a higher average score than the second class
function print(class1, class2) {    
    console.log(`First class got a ${class1 > class2 ? "higher" : "lower"} average score than the second class.`)
}

print(avgClassGrades1, avgClassGrades2);

