// Create a program where we have variables x and y coordinates representing the position of a robot, and a command string that tells the robot where it should move.
let x = 0; 
let y = 0;

// The robot has the following command string:
const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

for(i = 0; i <= commandList.length; i++) {
    const char = commandList.charAt(i);

    if(char === "C") {
        // do nothing
    } else if(char === "B") {
        break;
    } else if(char === "N") {
        y++;
    } else if(char === "E") {
        x++;
    } else if(char === "S") {
        y--;
    } else if(char === "W") {
        x--;
    }
}
console.log(`Final position of robot: x = ${x}, y = ${y}`);

// Depending on the letter, an action should be taken for each encountered letter:

// if the letter is N, increment Y
// if the letter is E, increment X
// if the letter is S, decrement Y
// if the letter is W, decrement X
// if the letter is C, skip to next letter without doing anything
// if the letter is B, end the program and skip all the remaining commands
// Print the final values of X and Y once the string has been processed.

// Tip: you can use the charAt function to get an individual letter by index, as is done in the code in assignment 6

// let x = 0;
// let y = 0;

// const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

// for (let i = 0; i < commandList.length; i++) {
//     const char = commandList.charAt(i);

//     if (char === 'B') {
//         break;
//     }

//     switch (char) {
//         case 'N':
//             y++;
//             break;
//         case 'E':
//             x++;
//             break;
//         case 'S':
//             y--;
//             break;
//         case 'W':
//             x--;
//             break;
//         case 'C':
//             break;
//         default:
//             throw new Error("Unknown command letter" + char);
//     }
// }

// console.log("Final position of robot: " + x + ", " + y);