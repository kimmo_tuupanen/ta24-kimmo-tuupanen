import Input from "./Input";
import "./App.css";
import UserInput from "./UserInput";
import FormInput from "./FormInput";
import ObjectState from "./ObjectState";
import CounterButtonsArray from "./CounterButtonsArray";

function App() {
  return (
    <>
      <h1 className="heading-primary">Exercise 1: User Input</h1>
      <Input />

      <h1 className="heading-primary">Exercise 2: User Input</h1>
      <UserInput />

      <h1 className="heading-primary">Exercise 3: Form Input</h1>
      <FormInput />

      <h1 className="heading-primary">Exercise 4: Object state</h1>
      <ObjectState />

      <h1 className="heading-primary">Exercise 5 - 12: Array of counters</h1>
      <CounterButtonsArray />
    </>
  );
}

export default App;
