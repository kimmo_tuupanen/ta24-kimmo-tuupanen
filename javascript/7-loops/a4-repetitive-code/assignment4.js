function checkSentenceVowels(sentence) {

const vowels = [
    {vowel: "a", count: countVowel(sentence, "a")},
    {vowel: "e", count: countVowel(sentence, "e")},
    {vowel: "i", count: countVowel(sentence, "i")}, 
    {vowel: "o", count: countVowel(sentence, "o")},
    {vowel: "u", count: countVowel(sentence, "u")},
    {vowel: "y", count: countVowel(sentence, "y")},
]

const totalCount = vowels.reduce((acc, cur) => acc + cur.count, 0);
console.log(`Total vowel count: ${totalCount}`);
};

// helper function to calculate vowels
function countVowel(string, vowel) {
    let count = 0;
    for (let i = 0; i < string.length; i++) {
        if (string.charAt(i).toLowerCase() === vowel) {
            count++;
        }
    }
    console.log(`${vowel.toUpperCase()} letter count: ${count}`);
    return count;
};

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");
