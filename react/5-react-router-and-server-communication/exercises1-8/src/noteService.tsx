interface Props {
  newObject: {
    id: number;
    date: string;
    content: string;
    important: boolean;
  };
  id?: number;
}

import axios from "axios";
const baseUrl = "http://localhost:3001/notes";

const getAll = async () => {
  const request = axios.get(baseUrl);
  const res = await request;
  return res.data;
};

const create = async ({ newObject }: Props) => {
  const request = axios.post(baseUrl, newObject);
  const res = await request;
  return res.data;
};

const update = async ({ id, newObject }: Props) => {
  const request = axios.put(`${baseUrl}/${id}`, newObject);
  const res = await request;
  return res.data;
};

export default { getAll, create, update };
