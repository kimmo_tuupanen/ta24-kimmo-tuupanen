{

const fruits: string[] = ["cherry",  "banana", "coconut", "apple", "pear", 
"pineapple", "lemon", "pumpkin"];

const loopedFruits: string[] = [];

for(const fruit of fruits) {
    if(fruit.length > 6) loopedFruits.push(fruit);
}

console.log(loopedFruits);

}