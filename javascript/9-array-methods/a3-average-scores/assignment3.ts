{
  interface Game {
    id: number;
    date: string;
    score: number;
    won: boolean;
  }

  const games: Game[] = [
    { id: 1586948654, date: "2022-10-27", score: 145, won: false },
    { id: 2356325431, date: "2022-10-30", score: 95, won: false },
    { id: 2968411644, date: "2022-10-31", score: 180, won: true },
    { id: 1131684981, date: "2022-11-01", score: 210, won: true },
    { id: 1958468135, date: "2022-11-01", score: 111, won: true },
    { id: 2221358512, date: "2022-11-02", score: 197, won: false },
    { id: 1847684969, date: "2022-11-03", score: 203, won: true },
  ];

  //   a)
  // Find all games that the player has won and calculate the player's average score in them. Print the average score.
  const playerWinAvgScore = games
    .filter((game) => game.won === true)
    .map((game) => game.score)
    .reduce((acc, cur, index, array) => acc + cur / array.length, 0); // 704 / 4 = 176

  console.log(playerWinAvgScore);

  // b)
  // Find all games that the player has lost and calculate the player's average score in them. Print the average score.
  const playerLostAvgScore = games
    .filter((game) => game.won === false)
    .map((game) => game.score)
    .reduce((acc, cur, index, array) => acc + cur / array.length, 0)
    .toFixed(1); // 437 / 3 = 145.7

  console.log(playerLostAvgScore);
}
