const num = 99;
const greet = 'Hello unknown!'
const idle = false;
const x = null;
let y;
 
console.log(num, greet, idle, x, y)
console.log(typeof(num))
console.log(typeof(greet))
console.log(typeof(idle))
console.log(typeof(x))
console.log(typeof(y))

// What is the type of a variable that has no assigned value?
// let y; // undefined