class Rectangle {
  constructor(public width: number, public height: number) {}

  getArea(): number {
    return this.width * this.height;
  }
}

const rectangleOne = new Rectangle(6, 3);
const rectangleTwo = new Rectangle(9, 5);

console.log(rectangleOne);
console.log(rectangleTwo);
console.log(rectangleOne.getArea());
