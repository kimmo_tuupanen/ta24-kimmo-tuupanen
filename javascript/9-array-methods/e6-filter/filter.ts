{
  const animals: string[] = [
    "horse",
    "cow",
    "dog",
    "hamster",
    "donkey",
    "cat",
    "parrot",
  ];

  // Create a program that finds us all animals that
  // a) have the letter ‘o’ in their name.
  const letterInName = animals.filter((animal) => animal.includes("o"));

  // b) don’t have a ‘h’ or ‘o’ in their name!
  const noLettersInName = animals.filter((animal) => {
    if (!animal.includes("h") && !animal.includes("o")) return animal;
  });

  // Print the array with the found animals.
  console.log(letterInName);
  console.log(noLettersInName);
}
