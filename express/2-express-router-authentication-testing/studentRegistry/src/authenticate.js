import "dotenv/config";
import jwt from "jsonwebtoken";

export const authenticate = (req, res, next) => {
  const auth = req.get("Authorization");
  if (!auth?.startsWith("Bearer ")) {
    return res.status(401).send("Invalid token");
  }
  const token = auth.substring(7);
  const secret = process.env.SECRET;
  try {
    const decodedToken = jwt.verify(token, secret);
    req.user = decodedToken;
    next();
  } catch (error) {
    return res.status(401).send("Invalid token");
  }
};
