const str =
  "According to all known laws of aviation, there is no way a bee should be able to fly.";

const words = str.toLowerCase().split(" ");
let startsWithA = [];
for (const word of words) {
  if (word.startsWith("a")) startsWithA.push(word);
}

console.log(startsWithA);
