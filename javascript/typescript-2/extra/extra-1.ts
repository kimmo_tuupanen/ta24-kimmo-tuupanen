const string = "adsf qwerty";

function createAscii(str: string) {
  const letters = str.split("").map((letter) => letter.charCodeAt(0));
  return letters;
}

console.log(createAscii(string));
// [97, 100, 115, 102, 32, 113, 119, 101, 114, 11, 121];
