const fruits = { banana: 118, apple: 85, mango: 200, lemon: 65 };

const printWeight = (fruit: string) => {
  for (const [key, value] of Object.entries(fruits)) {
    if (fruit === key) {
      console.log(`${key} weights ${value} grams.`);
    }
  }
  console.log(Object.keys(fruits));
};

printWeight("mango");
