// Create a program with two variables: a name and an age number. Assign some values for these variables.
const person = "John";
const age = 47;

// a) Make the program print the two variables.
console.log(person, age);

// b) Make the program print the line name is age years old.
console.log(person + " is " + age + " years old.");
console.log(`${person} is ${age} years old.`);

// Tip: To concatenate strings with variables, you can use the plus ( + ) operator. For example, console.log("Hello " + name).
