function getCountOfLetters(str: string) {
  const letters: Record<string, number> = {};

  for (const char of str) {
    if (char.trim() === "") continue;

    if (char in letters) {
      letters[char]++;
    } else {
      letters[char] = 1;
    }
  }
  return letters;
}

const result = getCountOfLetters("a black cat");
console.log(result);
