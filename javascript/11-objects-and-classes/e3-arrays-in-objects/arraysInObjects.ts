{
  interface Student {
    name: string;
    credits: number;
    courses: { name: string; grade: number }[];
  }

  // 1
  const student: Student = {
    name: "Kim",
    credits: 55,
    courses: [
      { name: "Advanced CSS and Sass", grade: 4 },
      { name: "JavaScript Basics", grade: 3 },
      { name: "Ultimate React Course", grade: 5 },
    ],
  };

  // 2
  const course = student.courses.find(
    (course) => course.name === "JavaScript Basics"
  );
  console.log(`${student.name} got ${course!.grade} from ${course?.name}`);

  // 3
  function addCourse(courseName: string, courseGrade: number) {
    student.courses.push({ name: courseName, grade: courseGrade });
  }
  addCourse("Complete JavaScript Course", 4);
  console.log(student);
}
