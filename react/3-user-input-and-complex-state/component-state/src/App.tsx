import "./App.css";
import TodoNote from "./TodoNote";
import InputForm from "./InputForm";
import { v4 as uuidv4 } from "uuid";
import { useState } from "react";
import SearchNote from "./SearchNote";

interface Todo {
  id: string;
  text: string;
  complete: boolean;
}

function App() {
  const [todos, setTodos] = useState<Todo[]>([
    { id: uuidv4(), text: "Buy potatoes", complete: false },
    { id: uuidv4(), text: "Make food", complete: false },
    { id: uuidv4(), text: "Exercise", complete: false },
    { id: uuidv4(), text: "Do the dishes", complete: false },
    { id: uuidv4(), text: "Floss the teeth", complete: false },
    { id: uuidv4(), text: "Play videogames", complete: true },
  ]);
  const [searchQuery, setSearchQuery] = useState("");

  function toggleCompletion(id: string) {
    setTodos((prevTodos) =>
      prevTodos.map((todo) =>
        todo.id === id ? { ...todo, complete: !todo.complete } : todo
      )
    );
  }

  function editTodo(id: string, newText: string) {
    setTodos((prevTodos) =>
      prevTodos.map((todo) =>
        todo.id === id ? { ...todo, text: newText } : todo
      )
    );
  }

  function removeTodo(id: string) {
    const newTodos = todos.filter((todo) => todo.id !== id);
    setTodos(newTodos);
  }

  function addTodo(text: string) {
    const newTodo: Todo = { id: uuidv4(), text, complete: false };
    setTodos([...todos, newTodo]);
    console.log(newTodo);
  }

  const todosToShow = todos.filter((todo) =>
    todo.text.toLowerCase().includes(searchQuery)
  );

  return (
    <>
      <div className="container">
        <InputForm addTodo={(text: string) => addTodo(text)} />
        <SearchNote setSearchQuery={setSearchQuery} />
        {todosToShow.map((todo) => (
          <TodoNote
            key={todo.id}
            todo={todo}
            onChange={() => toggleCompletion(todo.id)}
            onEdit={(id: string, newText: string) => editTodo(id, newText)}
            onDelete={() => removeTodo(todo.id)}
          />
        ))}
      </div>
    </>
  );
}

export default App;
