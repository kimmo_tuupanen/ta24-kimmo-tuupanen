const array = [ 1, 3, 4, 7, 11 ];

const insertNumber = (arr: number[], num: number): void => {
    for(let i = 0; i <= arr.length; i++) {
        if(num <= arr[i]) {
            array.splice(i, 0, num);
            return;
        }
    }
    array.push(num);
} 


insertNumber(array, 8);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11 ] 
insertNumber(array, 90);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11, 90 ]
