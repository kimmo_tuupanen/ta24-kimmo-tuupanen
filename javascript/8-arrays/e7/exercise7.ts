const numbers: number[] = [4, 7, 11, 5, 6, 9, 15, 7];
let biggerThanSeven: number[] = [];

for(let i = 0; i < numbers.length; i++) {
    if(numbers[i] < 7) continue;
    biggerThanSeven.push(numbers[i]);
}

console.log(biggerThanSeven);
