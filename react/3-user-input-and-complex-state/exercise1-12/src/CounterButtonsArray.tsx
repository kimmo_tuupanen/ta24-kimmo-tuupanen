import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import styled from "styled-components";
import CounterButton from "./CounterButton";

const Container = styled.div`
  display: flex;
  gap: 1.5rem;
  align-items: center;
  font-size: 1.6rem;
  margin-bottom: 2rem;
`;

type NumberItem = {
  id: number | string;
  count: number;
};

function CounterButtonsArray() {
  const [numbers, setNumbers] = useState<NumberItem[]>([
    { id: 0, count: 0 },
    { id: 1, count: 0 },
    { id: 2, count: 0 },
    { id: 3, count: 0 },
  ]);
  const [minimumNumber, setMinimumNumber] = useState<number>(0);
  const [showFiltered, setShowFiltered] = useState(false);

  function handleClick(index: number | string) {
    const newNumbers = numbers.map((number) => {
      if (number.id === index) {
        return { ...number, count: number.count + 1 };
      } else {
        return number;
      }
    });
    setNumbers(newNumbers);
  }

  const filteredNumbers = numbers.filter((item) => item.count >= minimumNumber);
  const numbersToShow =
    filteredNumbers.length > 0 && showFiltered ? filteredNumbers : numbers;
  const sum = numbersToShow.reduce((acc, cur) => acc + cur.count, 0);

  const handleMinimumNumber = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMinimumNumber(0);
    const value = parseInt(e.target.value, 10);
    if (!isNaN(value)) {
      setMinimumNumber(value);
    }
  };

  function handleAddNewCounter() {
    setNumbers([...numbers, { id: uuidv4(), count: 0 }]);
  }

  function handleDeleteCounter(id: number | string) {
    setNumbers(numbers.filter((number) => number.id !== id));
  }

  return (
    <>
      <Container>
        {numbersToShow.map((item) => (
          <CounterButton
            key={item.id}
            onClick={() => handleClick(item.id)}
            onDeleteCounter={() => handleDeleteCounter(item.id)}
            count={item.count}
          />
        ))}
        <div>equals {sum}</div>

        <label>
          <input
            type="checkbox"
            checked={showFiltered}
            onChange={() => setShowFiltered((show) => !show)}
          />
          Show Filtered Results
        </label>

        {showFiltered && (
          <input
            className="number-input"
            type="number"
            value={minimumNumber.toString()}
            onChange={handleMinimumNumber}
          />
        )}
      </Container>
      <button className="btn" onClick={handleAddNewCounter}>
        Add new Counterbutton!
      </button>
    </>
  );
}

export default CounterButtonsArray;
