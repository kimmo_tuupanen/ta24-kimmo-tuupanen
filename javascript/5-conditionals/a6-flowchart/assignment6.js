// Create a ATM program to check your balance. Create variables balance, isActive, checkBalance.

// Write conditional statements that implement the flowchart below.

// Change the values of balance, checkBalance, and isActive to test your code!

const balance = 135;
const checkBalance = true;
const isActive = true;


if(checkBalance) {
    if(isActive && balance > 0) {
        console.log(`Your balance: ${balance}€`)

    } else if(!isActive) {
        console.log("Your account is not active!")

    } else if(isActive && balance === 0) {
        console.log("Your account is empty!")
        
    } else {
        console.log("Your balance is negative.")
    }    
} else {
    console.log("Have a nice day!")
}

