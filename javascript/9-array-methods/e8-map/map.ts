{
  const animals: string[] = [
    "horse",
    "cow",
    "dog",
    "hamster",
    "donkey",
    "cat",
    "parrot",
  ];

  //   Using map,
  //   a) create a new array that contains the lengths of the animals’ names
  const nameLengths = animals.map((animal) => animal.length);

  //   b) create a new array that contains booleans that tell whether the specific     animals have the letter ‘o’ as their second letter
  const isTruthy = animals.map((animal) => animal.charAt(1).includes("o"));

  //   Print the original array as well as both arrays created with map.
  console.log(animals);
  console.log(nameLengths);
  console.log(isTruthy);
}
