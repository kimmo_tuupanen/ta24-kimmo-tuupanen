import DisappearingButton from "./DisappearingButton";

function Buttons() {
  return (
    <div className="btn-row">
      <DisappearingButton number={1} />
      <DisappearingButton number={2} />
      <DisappearingButton number={3} />
      <DisappearingButton number={4} />
      <DisappearingButton number={5} />
    </div>
  );
}

export default Buttons;
