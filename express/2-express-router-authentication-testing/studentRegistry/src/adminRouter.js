import express from "express";
import argon2 from "argon2";
import "dotenv/config";

const router = express.Router();

router.post("/admin", async (req, res) => {
  const { username, password } = req.body;
  const ADMIN = process.env.ADMIN;
  const HASH = process.env.HASH;

  const isValidUser = username === ADMIN;

  if (isValidUser) {
    const verified = await argon2.verify(HASH, password);
    if (verified) return res.status(204).end();
  }

  return res.status(401).end();
});

export default router;
