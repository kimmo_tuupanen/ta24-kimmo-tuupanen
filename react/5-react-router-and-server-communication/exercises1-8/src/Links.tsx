function Links() {
  return (
    <div>
      <p>
        <a href="https://www.dhiwise.com/post/a-comprehensive-guide-to-react-practical-exercises-and-coding-challenges#the-importance-of-practical-exercises-in-learning-react">
          The Importance of Practical Exercises in Learning React
        </a>
      </p>
      <p>
        <a href="https://www.dhiwise.com/post/a-comprehensive-guide-to-react-practical-exercises-and-coding-challenges#react-coding-challenges">
          React Coding Challenges
        </a>
      </p>
    </div>
  );
}

export default Links;
