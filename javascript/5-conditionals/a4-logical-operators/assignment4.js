// Create a program for checking whether a game was worth playing after completing it.
// Have three variables: score, hoursPlayed and price. Give them some values.

const score = 5;
const hoursPlayed = 10;
const price = 5;

if (score >= 4 && price === 0 || 
    score === 4 && hoursPlayed / price >= 4 ||
    score === 5 && hoursPlayed / price >= 2) {
    console.log(`Game cost ${price}€, scored ${score} and playing time was ${hoursPlayed} hours. Well worth the price!`)
} else {
    console.log(`Game cost ${price}€, scored ${score} and playing time was ${hoursPlayed} hours. NOT worth the money!`)
}

// A game is determined to be worth its price if:
// its score is at least 4 and it is free
// its score is 4 and its ratio of hours played to price is at least 4
// its score is 5 and its ratio of hours played to price is at least 2
// Games with scores less than 4 are never considered worth their price.

