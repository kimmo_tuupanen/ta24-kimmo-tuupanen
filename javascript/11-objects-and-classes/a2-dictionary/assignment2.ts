// a)

const englishToFinnish = {
  hello: "hei",
  world: "maailma",
  bit: "bitti",
  byte: "tavu",
  integer: "kokonaisluku",
  boolean: "totuusarvo",
  string: "merkkijono",
  network: "verkko",
};

// b)
function printTranslatableWords() {
  console.log(Object.keys(englishToFinnish));
}

printTranslatableWords();

// c)
function translate(word: string) {
  // @ts-ignore
  return englishToFinnish[word];
}
console.log(translate("byte"));
console.log(translate("integer"));

// d)
function translateTwo(word: string) {
  if (!(word in englishToFinnish)) {
    console.log(
      `No translation exists for word "${word}" given as the argument.`
    );
    return null;
  } else {
    // @ts-ignore
    return englishToFinnish[word];
  }
}
console.log(translateTwo("array"));
console.log(translateTwo("boolean"));
