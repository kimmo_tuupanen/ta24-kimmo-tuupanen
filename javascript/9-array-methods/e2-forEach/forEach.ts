const nums = [5, 13, 2, 10, 8];

function multiply(nums: number[]) {
  let total = 1;
  nums.forEach((num) => {
    total = num * total;
  });
  return total;
}

let agvSum = 0;
nums.forEach((num) => {
  agvSum += num / nums.length;
});

console.log(multiply(nums));
