import { GridObject } from "./App";

export function checkForBingo(grid: GridObject[][]): boolean {
  const gridSize = grid.length;

  // Check rows
  for (let i = 0; i < gridSize; i++) {
    if (grid[i].every((box) => box.isClicked)) {
      return true;
    }
  }

  // Check columns
  for (let j = 0; j < gridSize; j++) {
    let columnClicked = true;
    for (let i = 0; i < gridSize; i++) {
      if (!grid[i][j].isClicked) {
        columnClicked = false;
        break;
      }
    }
    if (columnClicked) return true;
  }

  // Check diagonals
  let diagonal1Clicked = true,
    diagonal2Clicked = true;
  for (let i = 0; i < gridSize; i++) {
    if (!grid[i][i].isClicked) diagonal1Clicked = false;
    if (!grid[i][gridSize - 1 - i].isClicked) diagonal2Clicked = false;
  }
  if (diagonal1Clicked || diagonal2Clicked) return true;

  return false;
}
