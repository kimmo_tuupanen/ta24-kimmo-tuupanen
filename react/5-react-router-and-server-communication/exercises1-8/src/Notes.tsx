import { useEffect, useState } from "react";
import noteService from "./noteService";
import Note from "./Note";

interface Note {
  id: number;
  content: string;
  date: string;
  important: boolean;
}

function Notes(): JSX.Element {
  const [notes, setNotes] = useState<Note[]>([]);

  useEffect(() => {
    noteService.getAll().then((initialNotes) => setNotes(initialNotes));
  }, []);

  function handlePostNote() {
    const newNote = {
      newObject: {
        id: (Math.random() * 1000).toFixed(),
        content: "Git is hard, screwing up is easy.",
        date: new Date().toISOString(),
        important: Math.random() > 0.5,
      },
    };

    //@ts-expect-error Type 'string' is not assignable to type 'number'.
    noteService.create(newNote).then((returnedNote) => {
      setNotes(notes.concat(returnedNote));
    });
  }

  function toggleImportance(id: number) {
    const note = notes.find((note) => note.id === id);
    if (!note) return;

    const changedNote = {
      ...note,
      important: !note!.important,
    };

    noteService.update({ id, newObject: changedNote }).then((returnedNote) => {
      setNotes(notes.map((note) => (note.id !== id ? note : returnedNote)));
    });
  }

  return (
    <div>
      {notes.map((note) => {
        return (
          <Note
            key={note.id}
            noteObj={note}
            toggleImportance={() => toggleImportance(note.id)}
          />
        );
      })}
      <button className="btn" onClick={handlePostNote}>
        Send a note!
      </button>
    </div>
  );
}

export default Notes;
