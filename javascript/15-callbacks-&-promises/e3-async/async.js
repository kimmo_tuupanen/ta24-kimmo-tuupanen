// Use the following asynchronous function twice to get 2 random values
// Do this exercise twice with both methods, async + await and promise.then()!

const getValue = function () {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ value: Math.random() });
    }, Math.random() * 1500);
  });
};

getValue()
  .then((value) => {
    console.log(value);
    return getValue();
  })
  .then((value) => {
    console.log(value);
  });

async function getIt() {
  let data = await getValue();
  console.log(data);
}

getIt();

// console.log(`Value 1 is ${valueOneHere} and value 2 is ${valueTwoHere}`);
