// a)

class Animal {
  constructor(public weight: number, public cuteness: number) {}
  makeSound() {
    console.log("silence");
  }
}

const animal = new Animal(6.5, 4.0);
animal.makeSound(); // prints "silence"
console.log(animal); // prints "Animal { weight: 6.5, cuteness: 4 }"

// b)

class Cat extends Animal {
  constructor(public weight: number, public cuteness: number) {
    super(weight, cuteness);
  }
  makeSound() {
    console.log("meooww!");
  }
}

const cat = new Cat(4.5, 3.0);
cat.makeSound(); // prints "meow"
console.log(cat); // prints "Cat { weight: 4.5, cuteness: 3 }"

// c)

class Dog extends Animal {
  constructor(weight: number, public cuteness: number, public breed: string) {
    super(weight, cuteness);
  }
  makeSound() {
    if (this.cuteness >= 4) {
      console.log("awoo");
    } else console.log("bark");
  }
}

const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
dog1.makeSound(); // prints "awoo"
dog2.makeSound(); // prints "bark"

//.js
// a)

// class Animal {
//   constructor(weight, cuteness) {
//     this.weight = weight;
//     this.cuteness = cuteness;
//   }
//   makeSound() {
//     console.log("silence");
//   }
// }

// const animal = new Animal(6.5, 4.0);
// animal.makeSound(); // prints "silence"
// console.log(animal); // prints "Animal { weight: 6.5, cuteness: 4 }"

// // B)

// class Cat extends Animal {
//   constructor(weight, cuteness) {
//     super(weight, cuteness);
//   }
//   makeSound() {
//     console.log("meooww!");
//   }
// }

// const cat = new Cat(4.5, 3.0);
// cat.makeSound(); // prints "meow"
// console.log(cat); // prints "Cat { weight: 4.5, cuteness: 3 }"

// // c)

// class Dog extends Animal {
//   constructor(weight, cuteness, breed) {
//     super(weight, cuteness);
//     this.breed = breed;
//   }
//   makeSound() {
//     if (this.cuteness >= 4) {
//       console.log("awoo");
//     } else console.log("bark");
//   }
// }

// const dog1 = new Dog(7.0, 4.5, "kleinspitz");
// const dog2 = new Dog(30.0, 3.75, "labrador");
// dog1.makeSound(); // prints "awoo"
// dog2.makeSound(); // prints "bark"
