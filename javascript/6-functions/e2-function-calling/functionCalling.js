function multiply(x, y) {
    return x * y;
}
let mySum = multiply(5, 8);
console.log(mySum);


const anonymous = function(x, y) {
    return x * y;
} 
mySum = anonymous(3, 8);
console.log(mySum);

const arrow = (x, y) => x * y;
console.log(arrow(7, 7));