interface UserInterface {
  name: string;
  width: number;
  height: number;
  children: any;
}

function buildUserInterface() {
  const mainWindow: UserInterface = {
    name: "MainWindow",
    width: 600,
    height: 400,
    children: [],
  };
  const buttonExit: UserInterface = {
    name: "ButtonExit",
    width: 100,
    height: 30,
    children: [],
  };
  mainWindow.children.push(buttonExit);

  const settingsWindow: UserInterface = {
    name: "SettingsWindow",
    width: 400,
    height: 300,
    children: [],
  };
  const buttonReturnToMenu: UserInterface = {
    name: "ButtonReturnToMenu",
    width: 100,
    height: 30,
    children: [],
  };
  settingsWindow.children.push(buttonReturnToMenu);
  mainWindow.children.push(settingsWindow);

  const profileWindow: UserInterface = {
    name: "ProfileWindow",
    width: 500,
    height: 400,
    children: [],
  };
  const profileInfoPanel: UserInterface = {
    name: "ProfileInfoPanel",
    width: 200,
    height: 200,
    children: [],
  };
  profileWindow.children.push(profileInfoPanel);
  mainWindow.children.push(profileWindow);

  return mainWindow;
}
const userInterfaceTree = buildUserInterface();

type ObjectOrNull = object | null;
function findControl(control: UserInterface, nameToFind: string): ObjectOrNull {
  if (control.name === nameToFind) {
    return control;
  }
  for (const child of control.children) {
    const result = findControl(child, nameToFind);
    if (result !== null) {
      return result;
    }
  }
  return null;
}

const profileInfoPanel = findControl(userInterfaceTree, "ProfileInfoPanel");
//@ts-ignore
profileInfoPanel.width += 100;
console.log(profileInfoPanel); // prints { name: 'ProfileInfoPanel', width: 300, height: 200, children: [] }
