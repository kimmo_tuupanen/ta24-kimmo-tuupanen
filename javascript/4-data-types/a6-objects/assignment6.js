// We have the following books:

// Dune, with 412 pages
// The Eye of the World, with 782 pages
// For each book, create an object that contains a name property, a pageCount property and a read property (indicating whether the book has been read). Give the properties fitting values.

const dune = {
    bookName: "Dune",
    pageCount: 412, 
    read: true,
}

const theEyeOfTheWorld = {
    bookName: "The Eye Of The World",
    pageCount: 782, 
    read: false,
}

// a)
// Print both of the book objects.
console.log(dune);
console.log(theEyeOfTheWorld);

// b)
// Swap the read status of both books so they're true (or, change them to false in case you originally assigned them as true). Print the books again.
dune.read = false;
theEyeOfTheWorld.read = true;
console.log(dune)
console.log(theEyeOfTheWorld)

// EXTRA: Instead of individual object variables, have the books in an array.
const myBooks = [dune, theEyeOfTheWorld];
console.log(myBooks);
