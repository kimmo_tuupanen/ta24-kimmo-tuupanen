function isPalindrome(str: any){ 
    for(let i = 0; i <= str.length; i++) {
        let char1 = str.charAt(i);
        let char2 = str.charAt(str.length - 1 - i);
    
        if(char1 !== char2) return false;
    }     

    return true;
}

// let value = isPalindrome("saippuakivikauppias");
// let value = isPalindrome("saippuakäpykauppias");
let value = isPalindrome("taidemediat");
// let value = isPalindrome("makkarakastike");

console.log(value);