let value = 2;
// console.log(value);
const increase = 3;
// console.log(increase);

value = value + increase; // 2 + 3 = 5
// console.log(value);
// console.log(increase);

// const newValue = value + increase;
// console.log(newValue);
// console.log(increase);
value = value + increase; // 5 + 3 = 8

const increaseMinusOne = increase - 1;
value = value + increaseMinusOne; // 8 + 2 = 10
console.log("result: " + value);
// console.log(increaseMinusOne);
