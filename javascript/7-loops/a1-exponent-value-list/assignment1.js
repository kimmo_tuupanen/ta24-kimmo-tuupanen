// a & b)

function exponentValueList(n, exponentValue) {
    if(n <= 0) {
        return console.log("n needs to be positive");    
    }
    for(i = 1; i <= n; i++) {      
        console.log(exponentValue ** i);
    }
};

exponentValueList(6, 3);