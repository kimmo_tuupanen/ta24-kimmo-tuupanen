import express from "express";
import argon2 from "argon2";
import "dotenv/config";
import jwt from "jsonwebtoken";

const router = express.Router();
let users = [];

router.post("/register", async (req, res) => {
  try {
    const { username, password } = req.body;
    const hashedPassword = await argon2.hash(password);
    users.push({ username, hashedPassword });

    const token = jwt.sign({ username: username }, process.env.SECRET, {
      expiresIn: "1h",
    });

    return res.status(200).json({ token: token });
  } catch (error) {
    res.status(500).send("Failed to register user.");
  }
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;
  const user = users.find((user) => user.username === username);

  if (user) {
    const verified = await argon2.verify(user.hashedPassword, password);
    const token = jwt.sign({ username: user.username }, process.env.SECRET, {
      expiresIn: "1h",
    });
    if (verified) return res.status(200).json({ token: token });
  }

  return res.status(401).end();
});

export default router;
