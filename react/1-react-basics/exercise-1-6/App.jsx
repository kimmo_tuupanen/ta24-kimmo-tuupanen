import { HelloWorld } from "./HelloWorld";
import { NameList } from "./NameList";

function App() {
  const currentYear = new Date().getFullYear();

  return (
    <>
      <HelloWorld name="React" year={currentYear} />
      <HelloWorld name="World" year={currentYear} />
      <HelloWorld name="You" year={currentYear} />
      <NameList />
    </>
  );
}

export default App;
