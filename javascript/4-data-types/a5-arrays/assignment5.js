
// a)
const studentAge = [20, 35, 27, 44];
console.log(studentAge);

// b)
const avgStudentAge = studentAge.reduce((acc, cur) => {
    return acc += cur / studentAge.length;
})

console.log(avgStudentAge); // 46.5