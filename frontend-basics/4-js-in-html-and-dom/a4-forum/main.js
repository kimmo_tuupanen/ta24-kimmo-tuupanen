const addPostButton = document.getElementById("addPost");
const submitButton = document.getElementById("submit");
const addPostForm = document.querySelector(".add-post-form");
const nameInput = document.getElementById("nameInput");
const postInput = document.getElementById("postInput");
const postContainer = document.querySelector(".post-container");
const deletePostButton = document.getElementById("delete");

addPostButton.addEventListener("click", () => {
  addPostForm.classList.toggle("visible");
});

submitButton.addEventListener("click", (e) => {
  e.preventDefault();
  addPost();
});

postContainer.addEventListener("click", function (e) {
  // Check if the clicked element is a delete button
  if (
    e.target &&
    e.target.classList.contains("btn") &&
    e.target.id === "delete"
  ) {
    // Find the closest parent `.post-item` and remove it
    const postItem = e.target.closest(".post-item");
    if (postItem) {
      postItem.remove();
    }
  }
});

function addPost() {
  const userName = nameInput.value;
  const postText = postInput.value;

  // Create new post element
  const postElement = document.createElement("div");
  postElement.classList.add("post-item");

  // Create HTML content for the post
  const postHTML = `
    <span class="post-item-heading">
      <p class="post-item-username">${userName}</p>
      <button class="btn" id="delete">Delete</button>
    </span>
    <p class="post-item-text">${postText}</p>
  `;

  postElement.innerHTML = postHTML;
  postContainer.appendChild(postElement);

  nameInput.value = "";
  postInput.value = "";
  addPostForm.classList.toggle("visible");
}
