const secondsParagraph = document.getElementById("seconds");

const timer = () => {
  let seconds = 50;
  let minutes = 0;
  setInterval(() => {
    seconds++;
    if (seconds >= 60) {
      seconds = 0;
      minutes++;
    }
    let time = `
      <p>${minutes > 0 ? `${minutes} minutes and` : ""} 
      ${seconds} seconds passed since last refresh... 🤓</p>
    `;
    secondsParagraph.innerHTML = time;
  }, 1000);
};

timer();
