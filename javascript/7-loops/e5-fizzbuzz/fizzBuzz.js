// Create a program that loops through numbers from 1 to 100 and...

// if the number is divisible by 3, prints “Fizz”
// if the number is divisible by 5, prints “Buzz”
// if the number is divisible by both (3 and 5), prints “FizzBuzz”
// if no previous conditions apply, prints just the number

let print = "";

for(let number = 1; number <= 100; number++) {
    if(number % 3 === 0 && number % 5 === 0) {
        print += "FizzBuzz";
        console.log(print);
        print = "";

    } else if(number % 3 === 0) {
        print += "Fizz";
        console.log(print);
        print = "";

    } else if(number % 5 === 0) {
        print += "Buzz";
        console.log(print);
        print = "";

    } else {
        console.log(number);
    }; 
}

// while loop
let number = 1;

while (number <= 100) {
    if(number % 3 === 0 && number % 5 === 0) {
        print += "FizzBuzz";
        console.log(print);
        print = "";

    } else if(number % 3 === 0) {
        print += "Fizz";
        console.log(print);
        print = "";

    } else if(number % 5 === 0) {
        print += "Buzz";
        console.log(print);
        print = "";

    } else {
        console.log(number);
    }; 
    number++;
}

