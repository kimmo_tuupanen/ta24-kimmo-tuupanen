import { useState } from "react";

// E3
const FormInput = () => {
  const [userInput, setUserInput] = useState("");
  const [displayText, setDisplayText] = useState("");

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUserInput(e.target.value);
  };

  const handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    setDisplayText(userInput);
    setUserInput("");
  };

  return (
    <>
      <h3>Your string is: {displayText}</h3>
      <form onSubmit={handleSubmit}>
        <input type="text" value={userInput} onChange={handleChange}></input>
        <button className="btn" type="submit">
          Submit
        </button>
      </form>
    </>
  );
};

export default FormInput;
