const names = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

const listStyle = {
  listStyle: "none",
};

export const NameList = () => {
  return (
    <ul style={listStyle}>
      {names.map((person, i) => (
        <li
          style={i % 2 === 0 ? { fontWeight: "bold" } : { fontStyle: "italic" }}
          key={person}
        >
          {person}
        </li>
      ))}
    </ul>
  );
};
