// a
const ages =  [20, 35, 27, 44, 24, 32];
console.log(ages);

// b
let ageSum = 0;
for(let i = 0; i < ages.length; i++) {
    ageSum += ages[i];
    
}
console.log(`Average age of students is ${(ageSum / ages.length).toFixed(1)} yrs.`);