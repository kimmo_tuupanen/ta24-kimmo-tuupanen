function factorial(n: number): number {
  if (n === 0) {
    return 1;
  } else {
    console.log(`Counting factorial. ${n}`);
    return n * factorial(n - 1);
  }
}
console.log(factorial(4));
