import * as fs from "fs";

function replaceWords(text) {
  // Replace "joulu" with "kinkku" and "lapsilla" with "poroilla"
  text = text.replace(/Joulu/g, "Kinkku");
  text = text.replace(/joulu/g, "kinkku");
  text = text.replace(/Lapsilla/g, "Poroilla");
  text = text.replace(/lapsilla/g, "poroilla");
  return text;
}

function modifyText() {
  // Read the content of the given file
  fs.readFile("textFile.txt", "utf8", (err, data) => {
    if (err) {
      console.error(err);
      return;
    }

    // Replace specified words
    const modifiedText = replaceWords(data);
    console.log(modifiedText);

    // Write altered text to a new file
    fs.writeFile("modifiedText.txt", modifiedText, "utf8", (err) => {
      if (err) {
        console.error(err);
        return;
      }
    });
  });
}

modifyText();
