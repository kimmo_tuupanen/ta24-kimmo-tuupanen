interface Props {
  newObject?: {
    id: string;
    text: string;
    complete: boolean;
  };
  id?: string;
}

import axios from "axios";
const baseUrl = "http://localhost:5173/notes";

const getAll = async () => {
  const request = axios.get(baseUrl);
  const res = await request;
  return res.data;
};

const create = async ({ newObject }: Props) => {
  const request = axios.post(baseUrl, newObject);
  const res = await request;
  return res.data;
};

const update = async ({ id, newObject }: Props) => {
  const request = axios.put(`${baseUrl}/${id}`, newObject);
  const res = await request;
  return res.data;
};

const deleteItem = async ({ id }: Props) => {
  const request = axios.delete(`${baseUrl}/${id}`);
  const res = await request;
  return res.data;
};

export default { getAll, create, update, deleteItem };
