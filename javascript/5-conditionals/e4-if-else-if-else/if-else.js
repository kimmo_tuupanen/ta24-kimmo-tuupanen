const isLoading = true;
const hasFinished = false;

if(isLoading && hasFinished) {
    console.log("both are true")

} else if(isLoading && !hasFinished) {
    console.log("first is true, second is false")

} else if(!isLoading && hasFinished) {
    console.log("first is false, second is true")
} else {
    console.log("both are false”")
}

