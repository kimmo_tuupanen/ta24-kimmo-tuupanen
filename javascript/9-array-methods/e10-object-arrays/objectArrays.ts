const users = [
  { firstName: "Bradley", lastName: "Bouley", role: "Full Stack Resident" },
  { firstName: "Chloe", lastName: "Alnaji", role: "Full Stack Resident" },
  { firstName: "Jonathan", lastName: "Baughn", role: "Enterprise Instructor" },
  { firstName: "Michael", lastName: "Herman", role: "Lead Instructor" },
  { firstName: "Robert", lastName: "Hajek", role: "Full Stack Resident" },
  { firstName: "Wes", lastName: "Reid", role: "Instructor" },
  { firstName: "Zach", lastName: "Klabunde", role: "Instructor" },
];

// Use the array methods to create new arrays.
// a) List the last names of all employees
const lastNames = users.map((user) => user.lastName);
console.log(lastNames);

// b) List all employee objects that are Full Stack Residents
const fullStackResidents = users.filter((user) => user.role.includes("Full"));
console.log(fullStackResidents);

// c) List all Instructors’ full names (e.g., “Wes Reid”)
const instructors = users
  .filter((user) => user.role === "Instructor")
  .map((user) => `${user.firstName} ${user.lastName}`);
console.log(instructors);
