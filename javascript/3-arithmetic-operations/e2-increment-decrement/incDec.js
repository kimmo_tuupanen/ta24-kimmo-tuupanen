// Create a program and assign a number to variable i.
let i = 9;

// Use increment operator to add to the number in variable i
// print the results using console.log
i++;
console.log(i);

// Use decrement operator to decrease the number in variable i and print the result
i--;
console.log(i);

// Try out both the postfix and the prefix operators!
const j = i++;
console.log(j);

const k = i--;
console.log(k);

const l = ++i;
console.log(l);

const m = --i;
console.log(m);
