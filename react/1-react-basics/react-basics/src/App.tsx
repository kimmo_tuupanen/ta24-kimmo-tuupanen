import "./App.css";
import { Greetings } from "./Greetings";
import Card from "./Card";
import Planets from "./Planets";

function App() {
  const fullName = "Kirk Hammett";
  const age = 61;

  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic" },
  ];

  return (
    <>
      <h1>Assignment 1: Simple props</h1>
      <Greetings name={fullName} age={age} />

      <h1>Assignment 2: HTML and CSS in React</h1>
      <Card />

      <h1>Assignment 3: Generating lists</h1>
      <Planets planets={planetList} />
    </>
  );
}

export default App;
