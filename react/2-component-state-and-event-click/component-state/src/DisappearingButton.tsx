import { useState } from "react";

type DisappearingButtonProps = {
  number: number;
};

function DisappearingButton({ number }: DisappearingButtonProps) {
  const [showButton, setShowButton] = useState(true);
  return (
    showButton && (
      <button className="btn" onClick={() => setShowButton((show) => !show)}>
        Button {number}
      </button>
    )
  );
}

export default DisappearingButton;
