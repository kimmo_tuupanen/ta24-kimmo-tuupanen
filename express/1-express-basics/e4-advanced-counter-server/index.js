import express from "express";
const server = express();

let userCounts = {};

server.use("/counter/:name", (request, res, next) => {
  const name = request.params.name;
  userCounts[name] = (userCounts[name] || 0) + 1;
  next();
});

server.get("/counter/:name", (request, response) => {
  const name = request.params.name;
  const count = userCounts[name] || 0;
  const times = count === 1 ? "time" : "times";
  response.send(`${name} was here ${count} ${times}`);

  console.log(userCounts);
});

server.listen(3000);
