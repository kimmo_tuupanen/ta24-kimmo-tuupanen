import express from "express";
import "dotenv/config";

import { logger } from "./logger.js";
import studentRouter from "./studentRouter.js";
import userRouter from "./userRouter.js";
import adminRouter from "./adminRouter.js";

const server = express();
server.use(express.json());

server.use(logger);
server.use(express.static("public"));
server.use("/students", studentRouter);
server.use("/", userRouter);
server.use("/", adminRouter);

// 404 error
server.use((req, res) => {
  res.status(404).json({ message: "URL Not Found" });
});

const { PORT } = process.env;
server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
