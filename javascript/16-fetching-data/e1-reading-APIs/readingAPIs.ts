import axios from "axios";
import { URL } from "../jsonplaceholder";

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

async function getPostById(id: number) {
  try {
    const response = await axios.get(`${URL}/posts/${id}`);
    const { userId, title } = response.data;
    const userName = await getUserById(userId);
    const userTotalPosts: Post[] = await getPostsByUser(userId);
    const postNumberByUser = userTotalPosts.findIndex((post) => post.id === id);

    console.log(`Post#[${id}]: "[${title}]"`);
    console.log(
      `Post#[${postNumberByUser + 1}] by [${userName}]: "[${title}]"`
    );
  } catch (error) {
    throw new Error("Failed to fetch post");
  }
}

async function getUserById(userId: number) {
  try {
    const response = await axios.get(`${URL}/users/${userId}`);
    return response.data.name;
  } catch (error) {
    throw new Error("Failed to fetch user");
  }
}

async function getPostsByUser(userId: number) {
  try {
    const response = await axios.get(`${URL}/posts?userId=${userId}`);
    const posts = response.data;
    return posts;
  } catch (error) {
    throw new Error("Failed to fetch posts");
  }
}

getPostById(75);
