// Create an object, that represents a boat, that has a values hullBreached: true/false and fillLevel: number between 0-100 and a method isItSinking() that checks if hullBreached is true/false and in case of true, starts filling the ship with 20% increments until full. Print the process out and as the ship is 100% full, create a new key value sunk: true and print the final object.

interface Boat {
  hullBreached: boolean;
  fillLevel: number;
  sunk?: boolean;
  isItSinking: () => void;
}

const boat: Boat = {
  hullBreached: true,
  fillLevel: 0,
  isItSinking() {
    while (true) {
      if (this.hullBreached && this.fillLevel < 100) {
        this.fillLevel += 20;
        console.log(`Boat is filling up ... ${this.fillLevel}%`);
      } else {
        boat.sunk = this.hullBreached ? true : false;
        console.log(boat);
        break;
      }
    }
  },
};

boat.isItSinking();
