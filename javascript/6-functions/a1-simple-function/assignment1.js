// a)
// Create a variable in the global scope for language.
// Then, create a function named hello. The function should print "Hello World!" in the specified language (for example, "fi", "en", "es"). At least 3 languages need to be supported.

// let language = "es";

// function hello() {
//     if(language === "fi") {
//         console.log("Hei maailma!")
//     } else if(language === "en") {
//         console.log("Hello world!") 
//     } else if(language === "es") {
//         console.log("Hola Mundo!")
//     } else {
//         console.log("Please specify language!")
//     }
// }
// hello();



// b)
// Remove the language variable from the global scope and instead add it as a parameter to your hello function.

// Call your hello function 3 times with different values for the language parameter to make your program print "Hello World!" to the console with 3 different languages.



function hello(lang) {
    if(lang === "fi") {
        console.log("Hei maailma!")
    } else if(lang === "en") {
        console.log("Hello world!") 
    } else if(lang === "es") {
        console.log("Hola Mundo!")
    } else {
        console.log("Please specify language!")
    }
}
hello("fi");
hello("en");
hello("es");
hello();