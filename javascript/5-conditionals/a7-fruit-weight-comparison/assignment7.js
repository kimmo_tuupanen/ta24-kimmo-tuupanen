// We have the following fruits:
// a pear, weighting 178 grams
// a lemon, weighting 120 grams
// an apple, weighting 90 grams
// a mango, weighting 150 grams
// Create objects for each fruit, with the object containing the fruit's name and its weight.

const fruits = [
{
    name: "pear", 
    weight: 178,
},
{
    name: "lemon", 
    weight: 120,
},
{
    name: "apple", 
    weight: 90,
},
{
    name: "mango", 
    weight: 150
}]


// Calculate the average weight of the fruits and print it.

const avgWeight = fruits.reduce((acc, cur) => 
    acc + cur.weight, 0) / fruits.length;

console.log(`Average weight of the fruits is ${avgWeight} grams.`);


// Programmatically compare the weight of each fruit to the average weight of the fruits.

function difference(arr) {
   arr.forEach((item) => {
    const weightDifference = Math.abs(item.weight - avgWeight);
    item.weightDifference = weightDifference;
    })
}

difference(fruits);

// create new array of weightDifferences
const diffs = fruits.map(item => item.weightDifference); // [ 43.5, 14.5, 44.5, 15.5 ]

// return the smallest of the numbers given as input parameters
const smallestDiff = Math.min(...diffs); // 14.5

// filter original array
const fruit = fruits.filter((fruit) => fruit.weightDifference === smallestDiff).at(0).name; // lemon
const fruitToUpperCase = fruit.charAt(0).toUpperCase() + fruit.slice(1); // Lemon

console.log(`${fruitToUpperCase} has a weight that is closest to the average.`);


