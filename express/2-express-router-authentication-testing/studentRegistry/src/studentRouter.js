import express from "express";
import { authenticate } from "./authenticate.js";

const router = express.Router();
router.use(authenticate);

let students = [];

//** POST *//

router.post("/", authenticate, (req, res) => {
  const { id, name, email } = req.body;

  if (!id || !name || !email) {
    return res.status(400).json({ message: "Missing or invalid parameters" });
  }
  students.push({ id, name, email });

  return res.status(201).end();
});

//** PUT *//

router.put("/:id", authenticate, (req, res) => {
  const paramsId = req.params.id;
  const { name, email } = req.body;
  if (!name && !email)
    return res.status(400).json({ message: "Name or email must be provided" });

  const student = students.find((student) => {
    return student.id === paramsId;
  });

  if (!student) {
    return res.status(404).json({ error: "Student not found" });
  }

  if (name) student.name = name;
  if (email) student.email = email;

  res.status(204).send();
});

//** DELETE *//

router.delete("/:id", authenticate, (req, res) => {
  const paramsId = req.params.id;

  const index = students.findIndex((student) => student.id === paramsId);
  if (index === -1) {
    return res.status(404).json({ error: "Student not found" });
  }

  students.splice(index, 1);
  res.status(204).send();
});

//** GET *//

router.get("/", authenticate, (req, res) => {
  res.json(students.map((student) => student.id));
});

router.get("/:id", authenticate, (req, res) => {
  const paramsId = req.params.id;
  const student = students.find((student) => {
    return student.id === paramsId;
  });

  if (!student) {
    return res.status(404).json({ message: "Student not found" });
  }
  return res.json(student);
});

export default router;
